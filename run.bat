@echo off

cd bin
chcp 65001

set /p num= How many orders(min 0, max 2000)?
java simulation.Program "%num%" %*
pause