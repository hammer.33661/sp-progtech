package data;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import structure.City;
import structure.Order;

/**
 * Model of an area containing cities and the roads connecting
 * them. An instance of this class contains the cities residing
 * in the model. This class also contains methods for searching
 * in the model. 
 */
public class AreaModel {


    /** Cities in this model */
    private static Map<Integer, City> cityMap;
    
    private static int connections;



    //
    //                ===   PATH SEARCHING   ===
    //

    /**
	 * @return the cityMap
	 */
	public static Map<Integer, City> getCityMap() {
		return cityMap;
	}

	/**
	 * @param cityMap the cityMap to set
	 */
	public static void setCityMap(Map<Integer, City> cityMap) {
		AreaModel.cityMap = cityMap;
	}

	/**
	 * @return the connections
	 */
	public static int getConnections() {
		return connections;
	}

	/**
	 * @param connections the connections to set
	 */
	public static void setConnections(int connections) {
		AreaModel.connections = connections;
	}

	/**
     * Performs a search through the graph and finds the shortest path
     * from city {@code start} to city{@code finish} using
     * Dijkstra's algorithm.
     *
     * @param start   Starting city
     * @param finish  End city
     * @param costs   Costs of distance and time spent on road
     *
     * @return A list of cities in order of shortest path traversal.
     *         {@code null} in case we couldn't reach finish in 
     *         the graph.
     *
     * @see <a href='www.baeldung.com/java-dijkstra'>
     *     Dijkstra algorithm in Java | Baeldung</a>
     */
    private ArrayList<City> dijkstraShortestPath(
            City start, City finish, RoadData.Costs costs) {

        // INITIALIZATION

        cityMap.values().forEach(
                city -> {
                    city.setCost(Integer.MAX_VALUE);
                    city.setShortestPath(new ArrayList<>());
                });
        start.setCost(0);

        Set<City> settled = new HashSet<>();
        Set<City> unsettled = new HashSet<>();

        unsettled.add(start);

        // END INIT


        //// long SS = System.currentTimeMillis();
        
        while (!unsettled.isEmpty()) {

            // Get least-costly unsettled node
            City currentNode = lowestCostCityFrom(unsettled);
            unsettled.remove(currentNode);

            // Go through least-distant's node's neighbours
            for (Map.Entry<City, RoadData> neighbour :
                    currentNode.getNeighbours().entrySet()) {

                // Get node and costs
                City adjNode = neighbour.getKey();
                RoadData adjCosts = neighbour.getValue();

                // If the neighbour is finish, terminate
                if (adjNode.equals(finish)) {
                    //// System.out.format("Found finish after %d ms\n", System.currentTimeMillis()-SS);
                    ArrayList<City> path = currentNode.getShortestPath();
                    path.add(finish);
                    return path;
                }
                
                // If adjacent isn't settled yet
                if (!settled.contains(adjNode)) {

                    // Try traversing to adjacent node from current node
                    updateScore(adjNode, adjCosts, currentNode, costs);

                    unsettled.add(adjNode);
                }

            }

            // Mark the least-distant node settled
            settled.add(currentNode);
        }
        
        //// System.out.format("Looking up road took %d ms\n", System.currentTimeMillis()-SS);

        // If every node has been checked but none were equal
        // to finish, we couldn't reach the finish from start
        return null;
    }

    /**
     * Reads through a set of cities and returns the city with the lowest cost.
     *
     * @param cities  A set of cities with costs
     * @return        The city with the lowest cost
     */
    private static City lowestCostCityFrom(Set<City> cities) {

        City best = null;
        int lowestCost = Integer.MAX_VALUE;

        for (City n : cities) {
            int cost = n.getCost();
            if (cost < lowestCost) {
                best = n;
                lowestCost = cost;
            }
        }

        return best;
    }

    /**
     * Updates the cost and shortest path of {@code evalCity} if the edge
     * from {@code tryCity} to {@code evalCity} is shorter (lower cost) than
     * the existing one; i.e. if going through {@code tryCity} is better,
     * change {@code evalCity}.
     *
     * @param evalCity   City we're trying to get to
     * @param road       Road between the two cities
     * @param tryCity    City we're trying to go from
     * @param costs      Costs of distance and time
     */
    private static void updateScore(
            City evalCity, RoadData road, City tryCity,
            RoadData.Costs costs) {

        if (costs == null) costs = RoadData.Costs.SHORTEST;

        int newCost = tryCity.getCost() + road.getCost(costs);

        if (newCost < evalCity.getCost()) {

            ArrayList<City> shortestPath =
                    new ArrayList<City>(tryCity.getShortestPath());
            shortestPath.add(evalCity);
            evalCity.setShortestPath(shortestPath);
            evalCity.setCost(newCost);

        }
    }





    /**
     * Finds the best path between two cities for given costs
     * of distance and time.
     *
     * @param a      Starting city
     * @param b      Ending city
     * @param costs  Costs of distance and time
     * @return  A linked list of cities in order of traversal.
     */
    ArrayList<City> getWeightedPathBetween(City a, City b, RoadData.Costs costs) {
        return dijkstraShortestPath(a, b, costs);
    }

    /**
     * Finds the shortest path between two cities;
     * ie. path with shortest distance.
     *
     * @param a Starting city
     * @param b Ending city
     * @return  A linked list of cities in order of traversal.
     */
    public ArrayList<City> getShortestPathBetween(City a, City b) {
        return getWeightedPathBetween(a, b, RoadData.Costs.SHORTEST);
    }

    /**
     * Finds the fastest path between two cities;
     * ie. path traversed the quickest.
     *
     * @param a Starting city
     * @param b Ending city
     * @return  A linked list of cities in order of traversal.
     */
    ArrayList<City> getFastestPathBetween(City a, City b) {
        return getWeightedPathBetween(a, b, RoadData.Costs.FASTEST);
    }
    
    /**
     * Returns length in kms of given path through Cities
     * @param pth given path
     * @return length of given path in kms
     */
    int getLengthOfPath(ArrayList<City> pth) {
    	int ret = 0;
    	
    	for(int i = 0; i < pth.size() - 1; i++) {
    		if(pth.get(i).getNeighbours().containsKey(pth.get(i + 1))) {
    			ret += pth.get(i).getNeighbours().get(pth.get(i + 1)).getDistance() ;
    		}
    		else {
    			System.out.println("Zadaná cesta pro nalezení délky cesty není validní.");
    		}
    		
    	}
    	
    	return ret;
    }
    
    /**
     * Returns speed in kms/h of given path through Cities
     * 
     * @param pth given path
     * @return length of given path in kms/h
     */
    int getSpeedOfPath(ArrayList<City> pth) {
    	int ret = 0;
    	
    	for(int i = 0; i < pth.size() - 1; i++) {
    		if(pth.get(i).getNeighbours().containsKey(pth.get(i + 1))) {
    			ret += pth.get(i).getNeighbours().get(pth.get(i + 1)).getSpeed() ;
    		}
    		else {
    			System.out.println("Zadaná cesta pro nalezení rychlosti cesty není validní.");
    		}
    		
    	}
    	
    	return ret;
    }

    /**
     * Fills in two matrices representing a distance matrix and 'speed' matrix.
     * Fills in road costs for neigbouring cities, 0 for i = j indexes and
     * infinity when there's no immediate connection between two cities.
     * 
     * @return ArrayList of two matrices (2D arrays)
     */
    public static ArrayList<int[][]> fillWeights() {
    	ArrayList<int[][]> result = new ArrayList<>();
    	
    	int [][] dist = new int[cityMap.size()][cityMap.size()];
    	int [][] sp = new int[cityMap.size()][cityMap.size()];
    	int costD = 0;
    	int costS = 0;
    	
    	for(int i = 0; i < dist.length; i++) {
    		for(City n : cityMap.get(i).getNeighbours().keySet()) {
    			costD = cityMap.get(i).getNeighbours().get(n).getDistance();
    			costS = cityMap.get(i).getNeighbours().get(n).getSpeed();
    			
    			dist[i][n.getId()] = costD;
    			dist[n.getId()][i] = costD;
    			
    			sp[i][n.getId()] = costS;
    			sp[n.getId()][i] = costS;
    		}
    	}
    	
    	
    	for(int i = 0; i < dist.length; i++) {
    		for(int j = i; j < dist[0].length; j++) {
    			if(dist[i][j] == 0 && j != i) {
    				dist[i][j] = Integer.MAX_VALUE;
    				dist[j][i] = Integer.MAX_VALUE;
    				
    				//budou to mít na stejných indexech
    				sp[i][j] = Integer.MAX_VALUE;
    				sp[j][i] = Integer.MAX_VALUE;
    			}
    		}
    	}
    	result.add(dist);
    	result.add(sp);
    	
    	return result;
    }




    /**
     * A shorthand method for testing existence of city in {@code cityMap}.
     *
     * @param m  Searched city
     * @return   Does this graph contain the city?
     */
    public boolean contains(City m) {
        return cityMap.values().contains(m);
    }

    /**
     * A getter for retrieving a city from the {@code HashMap} of {@code City}
     * instances.
     * @param id  identifier of city
     * @return    {@code City} with searched identifier
     */
    public static City get(int id) {
        return cityMap.get(id);
    }
    
    /**
     * Getter method for retrieving an object from 
     * the {@code HashSet&lt;City&gt;} with given ID. 
     * 
     * @param id   ID number of the searched-for {@code City}
     * @param set  A set of cities to search in 
     * @return  A city with the given ID number
     */
    public static City get(int id, Set<City> set) {
    	return (City) set.stream()
                .filter(c -> id == c.getId())
                .findAny()
                .orElse(null);
    }





    //
    //                  ===   Generování modelu   ===
    //
    /**
     * Generates AreaModel from a file given by a filepath
     * @param filepath filepath to a file with world data
     * @return new AreaModel
     */
    public static AreaModel generateFromFile(String filepath) {
        AreaModel model = null;
        try {
            
            model = generateFromFile(
                    new BufferedReader(
                            new FileReader(
                                    filepath)));
        } catch (Exception e) {
            return null;
        }
        return model;
    }

    /**
     * Creates a map of the "world" (aka the Czech Republic) with 2000
     * cities and (at least 200) paths between each of them
     * @param rd file with the data
     * @return new AreaModel
     * @throws IOException if there is a problem with file reading
     */
    public static AreaModel generateFromFile(BufferedReader rd) throws IOException {
        AreaModel model = new AreaModel();
        cityMap = new HashMap<>(2000);
        
        City city1;
        City city2;
        int distance;
        int speed;
        
        String splits[] = new String[4];
        String idAndName[] = new String[2];
        String pathsInfo[] = new String[4]; 
        
        //setting the reader to the right position
        rd.readLine();
        rd.readLine();
        String line = rd.readLine();

        //Creates cities and adds them to HashSet
        // #TODO consider using regex to speed up / improve security
        for(int i = 0; i < 2000; i++) {
        	splits = line.split(",");
        	idAndName = splits[0].split(":");

        	String cityName = idAndName[1];
        	int id = Integer.parseInt(idAndName[0]);
        	int maxPallets = Integer.parseInt(splits[1]);
        	int opens = Integer.parseInt(splits[2]);
        	int closes = Integer.parseInt(splits[3]);
        	
        	cityMap.put(id, new City(cityName, id, maxPallets, opens, closes));
        	
        	line = rd.readLine();
        	
        }
        
        //setting the reader to the right position
        line = rd.readLine();
        
        
        //connecting
        // TODO - resolve case when get(int id, HashSet<City> set) returns null (shouldn't happen though)
        while(line != null) {
        	//ind 2 je vzdalenost
        	pathsInfo = line.split(",");
        	
        	city1 = get(Integer.parseInt(pathsInfo[0]));
        	city2 = get(Integer.parseInt(pathsInfo[1]));

        	distance = Integer.parseInt(pathsInfo[2]);
        	speed = Integer.parseInt(pathsInfo[3]);
        	RoadData road = new RoadData(distance, speed);

        	city1.connectTo(city2, road);
        	
        	line = rd.readLine();
        	connections++;
        }
        
        return model;
    }
    
    /**
     * Generates random orders from unique cities
     * @param numberOfOrders number of orders to be generated
     * @return List of orders
     */
    public LinkedList<Order> generateOrders(int numberOfOrders){
    	LinkedList<Order> orders = new LinkedList<>();
    	ArrayList<City> orderingCities = new ArrayList<>();
    	Random rand = new Random();
    	int randId = 0;
    	
    	for(int i = 0; i < numberOfOrders; i++) {
    		randId = rand.nextInt(2000);
    		orderingCities.add(get(randId));
    	}
    	
    	for(City city: orderingCities) {
    		orders.add(new Order(city, city.getMaxPallets()));
    	}

		return orders;
    }
    
    /**
     * Generates always the same orders from unique cities - used for debugging
     * @param numberOfOrders number of orders to be generted
     * @return List of orders
     */
    public LinkedList<Order> generateNotRandomOrders(int numberOfOrders){
    	LinkedList<Order> orders = new LinkedList<>();
    	ArrayList<City> orderingCities = new ArrayList<>();
    	
    	for(int i = 1000; i < numberOfOrders + 1000; i++) {
    		orderingCities.add(get(i));
    	}
    	
    	for(City city: orderingCities) {
    		orders.add(new Order(city, city.getMaxPallets()));
    	}

		return orders;
    }



    public String statistics() {
        // #TODO give statistics of model (avg road length, biggest city...)
        return "";
    }

}



