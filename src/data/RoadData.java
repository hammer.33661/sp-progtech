package data;

import structure.City;

/**
 * A container for weights between two instances of {@link City}.
 * This class can be used to get the cost of traversal along this road. 
 */
public class RoadData {

    /** The distance to a city (in km) */
    private int distance;
    

	/** The velocity of travel to city (in km/h) */
    private int speed;
    
    
    /**
	 * @return the distance
	 */
	public int getDistance() {
		return distance;
	}

	/**
	 * @param distance the distance to set
	 */
	public void setDistance(int distance) {
		this.distance = distance;
	}

	/**
	 * @return the speed
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * @param speed the speed to set
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
	}


    /**
     * Makes a weight for a {@link City} to be used in calculating
     * the cost of travelling to the {@link City}.
     *
     * @param distance  The distance to a {@link City} (in km)
     * @param speed     The velocity of travel to {@link City} (in km/h)
     */
    public RoadData(int distance, int speed) {

        if (distance <= 0)
            throw new IllegalArgumentException("Distance has to be a non-zero positive int ("+ distance +").");
        if (speed <= 0)
            throw new IllegalArgumentException("Speed has to be a non-zero positive int ("+ speed +").");

        this.distance = distance;
        this.speed = speed;
    }

    /**
     * Returns a cost of travelling to a {@link City} based on the cost
     * of traversing this road.
     *
     * @param cost  Costs of distance and time spent on road
     * @return      The cost of travelling across this road
     */
    int getCost(Costs cost) {

        int timeHrs = speed / distance;
        int timeMin = timeHrs * 60;

        return (int) (distance * cost.perKilometer + timeMin * cost.perMinute);
    }

    /**
     * An instance of {@code Parameter} represents a messenger with factors
     * for distance and speed to be later used to calculate a cost of edge
     * between nodes.
     */
    static class Costs {

        /** A cost with an absolute preference of shortest distance */
        public static final Costs SHORTEST = new Costs(1, 0);
        /** A cost with an absolute preference of fastest traversal */
        public static final Costs FASTEST  = new Costs(0, 1);

        /** Cost of traversing one kilometer of road */
        int perKilometer;
        /** Cost of travelling for one minute */
        int perMinute;

        /**
         * Messenger with prices of one kilometer of road and one minute
         * spent on road.
         *
         * @param pricePerKilometer A price of traversing one kilometer of road
         * @param pricePerMinute    A price of travelling for one minute
         */
        Costs(int pricePerKilometer, int pricePerMinute) {

            if (pricePerKilometer < 0)
                throw new IllegalArgumentException("Error road costs: Price of a kilometer has to be positive ("+ pricePerKilometer +").");
            if (pricePerMinute < 0)
                throw new IllegalArgumentException("Error road costs: Price of a minute has to be positive ("+ pricePerMinute +").");

            this.perKilometer = pricePerKilometer;
            this.perMinute = pricePerMinute;
        }

    }
}
