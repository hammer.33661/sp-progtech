package simulation;

/**
 * Instances of this class hold simulation time information 
 * for various events that occur in the simulation. 
 * The instances can be thought of as timestamps. 
 */
public class SimulationTime implements Comparable<SimulationTime> {
    
    /** Minutes */
    private int mins;
    /** Hours */
    private int hrs;
    /** Days */
    private int day;

    /**
	 * @return the mins
	 */
	public int getMins() {
		return mins;
	}

	/**
	 * @param mins the mins to set
	 */
	public void setMins(int mins) {
		this.mins = mins;
	}

	/**
	 * @return the hrs
	 */
	public int getHrs() {
		return hrs;
	}

	/**
	 * @param hrs the hrs to set
	 */
	public void setHrs(int hrs) {
		this.hrs = hrs;
	}

	/**
	 * @return the day
	 */
	public int getDay() {
		return day;
	}

	/**
	 * @param day the day to set
	 */
	public void setDay(int day) {
		this.day = day;
	}

	/**
     * An instance of {@link SimulationTime} is a timestamp
     * in the simulation. 
     * @param hours    Hours in the simulation
     * @param minutes  Minutes in the simulation
     * @param days     Days in the simulation
     */
    public SimulationTime(int hours, int minutes, int days) {
        this.hrs  = hours;
        this.mins = minutes;
        this.day  = days;
        checkOverflow();
    }

    /**
     * Shorthand for {@link #SimulationTime(int,int,int)}.
     * @param hours   Hours
     * @param minutes Minutes
     */
    public SimulationTime(int hours, int minutes) {
        this(hours, minutes, 0);
    }
    
    /**
     * Shorthand for {@link #SimulationTime(int,int,int)}.
     * @param t  Instance of simulation time to be copied. 
     */
    public SimulationTime(SimulationTime t) {
        this(t.hrs, t.mins, t.day);
    }
    
    /**
     * Copies the time in {@code t} and adds the number of minutes
     * to the time. 
     * See {@link #SimulationTime(int,int,int)} for basic function.
     * @param t  
     *     Instance of simulation time to get the offset
     * @param minuteOffset 
     *     Amount of the offset 
     */
    public SimulationTime(SimulationTime t, int minuteOffset) {
        this(t.hrs, t.mins + minuteOffset, t.day);
    }

    /**
     * Adds a number of minutes to the time. 
     * @param elapsedMinutes  Number of minutes to be added
     */
    public void addMins(int elapsedMinutes) {
        mins += elapsedMinutes;
        checkOverflow();
    }

    /** Adds a number of hours to the time.
     * @param elapsedHours  Number of hours to be added
     */
    public void addHours(int elapsedHours) {
        hrs += elapsedHours;
        checkOverflow();
    }

    /**
     * Checks the hours and minutes if they have overflown
     * values (minutes must be less than 60 etc.) 
     */
    private void checkOverflow() {
        if (mins >= 60) {
            hrs  += mins / 60;
            mins %= 60;
        }
        if (hrs >= 24) {
            day += hrs / 24;
            hrs %= 24;
        }
    }

    /**
     * Getter for the total minutes (including days) of the time. 
     * @return  Total number of minutes elapsed (including days)
     */
    public int getTotalMins() {
        return (getDayMins() + day*60*24);
    }

    /**
     * Getter for the minutes elapsed in current day. 
     * @return  Total number of minutes elapsed (current day only)
     */
    public int getDayMins() {
        return (mins + hrs*60);
    }

    /**
     * Checks if this instance falls into an interval given by the parameter. 
     * This method does not work with days, only with time of day. 
     * 
     * @param intervalStart  Timestamp of interval start
     * @param minuteLength   Interval length
     * @return  Does this time of day fall into the interval [start; start+minuteLength)? 
     */
    public boolean collidesWith(SimulationTime intervalStart, int minuteLength) {
        int start = intervalStart.getDayMins();
        int end = start + minuteLength;
        int checked = this.getDayMins();
        return (start <= checked && checked < end);
    }

    @Override
    public String toString() {
        return String.format("%02d:%02d, day %d", hrs, mins, day);
    }

    @Override
    public int compareTo(SimulationTime other) {
        int thisTime = this.getTotalMins();
        int otherTime = other.getTotalMins();
        return (thisTime - otherTime);  // Get ascending, lowest time to highest
    }
}
