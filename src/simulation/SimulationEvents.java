package simulation;
import java.util.ArrayList;
import java.util.Collections;

/**
 * The class instances represent events that occurred during 
 * the simulation. 
 */
public class SimulationEvents {
    
    /** List of event objects */
    private ArrayList<Event> eventList;
    
    /** Preserved flushed event entries */
    private ArrayList<Event> history;

    /**
     * Creates a new container for simulation events. 
     */
    public SimulationEvents() {
        history = new ArrayList<>();
        eventList = new ArrayList<>();
    }

    /**
     * Removes the events from the {@link #eventList} 
     * and saves them to the {@link #history} list. 
     */
    void flushEventList() {
        history.addAll(getEvents());
        eventList = new ArrayList<>();
    }

    /**
     * Adds a simulation event to the list. 
     * @param time          Time of the event (timestamp)
     * @param type          Type (new order, car unloading etc.)
     * @param description   Description of the event
     */
    public void add(SimulationTime time, Type type, String description) {
        
        if (time == null || type == null) 
            throw new IllegalArgumentException("Event time and event type cannot be null.");
        
        eventList.add(new Event(time, type, description));
    }

    /**
     * Gets the list of events sorted by time of occurrence. 
     * @return List of events sorted by time. 
     */
    ArrayList<Event> getEvents() {
        return eventList;
    }
    
    /**
     * Gets the list of events sorted by time of occurrence (most recent first)
     * @return List of events sorted by time. 
     */
    public ArrayList<Event> getEventsSortedByTime() {
        ArrayList<Event> events = getEvents();
        Collections.sort(events);  // Collections sort -> using Event.compareTo(Event)
        return events;
    }

    /**
     * A container object for an event. 
     * Contains the event's time, type and description. 
     */
    public class Event implements Comparable<Event>{
        private SimulationTime time;
        private Type type;
        private String description;
        
        private Event(SimulationTime time, Type type, String description) {
            this.time = time;
            this.type = type;
            this.description = description;
        }
        
        public Type getType() {
            return type;
        }
        
        public String getDescription() {
            return description;
        }

        @Override
        public String toString() {
            return String.format(
                    "[%s] %s:\n" +
                    "  %s\n", 
                    time, type.prefix, description);
        }

        @Override
        public int compareTo(Event compared) {
            return this.time.compareTo(compared.time);
        }
    }

    /**
     * The various types of events that can occur. 
     * This is later used to filter the types of events. 
     */
    public enum Type {
        
        CAR_PROGRESS("Car progress"), CAR_DONE("Car finished"), 
        CAR_INITIATED_PATH("Car departed"), CAR_UNLOADING("Car unloading"),
        ORDER_RESOLVED("Order resolved"), ORDER_ERROR("Order complication"), 
        ORDER_NEW("Order accepted"), ORDER_NEW_MORNING("Morning orders"), 
        DAY_SUMMARY("Summary of completed day");
        
        /** Prefix of the type, used in output */
        String prefix;

        /**
         * Creates a new event type
         * @param prefix  The type's prefix
         */
        Type(String prefix) {
            this.prefix = prefix;
        }
    }
    
}
