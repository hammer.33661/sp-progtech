package simulation;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Allows for using multiple output streams at once,
 * for ex. writing to {@code System.out} and a file
 * at the same time.
 */
public class DupingOutputStream extends OutputStream {

    private OutputStream[] outputStreams;

    /**
     * Creates a stream that writes to all passed streams
     * at the same time.
     * @param streams duplicate streams to wirte to
     */
    public DupingOutputStream(OutputStream[] streams) {
        this.outputStreams = streams;
    }

    @Override
    public void write(int b) throws IOException {
        for (OutputStream out: outputStreams)
            out.write(b);
    }

    @Override
    public void write(byte[] b) throws IOException {
        for (OutputStream out: outputStreams)
            out.write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        for (OutputStream out: outputStreams)
            out.write(b, off, len);
    }

    @Override
    public void flush() throws IOException {
        for (OutputStream out: outputStreams)
            out.flush();
    }

    @Override
    public void close() throws IOException {
        for (OutputStream out: outputStreams)
            out.close();
    }

}

