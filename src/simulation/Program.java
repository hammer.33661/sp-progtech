package simulation;
import java.io.*;

import data.AreaModel;
import structure.FloydWarshall;

/**
 * 
 * @author Eliska Mourycová; Milan Kladivko
 */
public class Program {
	
    /** File containing generated model data */
	private static final String DATA_FILE = "generator/data.txt";
	/** Hard coded ID code of Prague in data file */
    private static int PRAGUE_FILE_ID = 1351;

    /** The file used for logging of console output */
    private static String LOGGING_FILE = "log.txt";
    
    private static int[][] allDistances;
    private static int[][] allSpeeds;


    /**
	 * @return the pRAGUE_FILE_ID
	 */
	public static int getPRAGUE_FILE_ID() {
		return PRAGUE_FILE_ID;
	}


	/**
	 * @param pRAGUE_FILE_ID the pRAGUE_FILE_ID to set
	 */
	public static void setPRAGUE_FILE_ID(int pRAGUE_FILE_ID) {
		PRAGUE_FILE_ID = pRAGUE_FILE_ID;
	}


	/**
	 * @return the lOGGING_FILE
	 */
	public static String getLOGGING_FILE() {
		return LOGGING_FILE;
	}


	/**
	 * @param lOGGING_FILE the lOGGING_FILE to set
	 */
	public static void setLOGGING_FILE(String lOGGING_FILE) {
		LOGGING_FILE = lOGGING_FILE;
	}


	/**
	 * @return the allDistances
	 */
	public static int[][] getAllDistances() {
		return allDistances;
	}


	/**
	 * @param allDistances the allDistances to set
	 */
	public static void setAllDistances(int[][] allDistances) {
		Program.allDistances = allDistances;
	}


	/**
	 * @return the allSpeeds
	 */
	public static int[][] getAllSpeeds() {
		return allSpeeds;
	}


	/**
	 * @param allSpeeds the allSpeeds to set
	 */
	public static void setAllSpeeds(int[][] allSpeeds) {
		Program.allSpeeds = allSpeeds;
	}


	/**
	 * @return the dataFile
	 */
	public static String getDataFile() {
		return DATA_FILE;
	}


	/**
     * Starts the program by loading the input model data file. 
     * Then gives the control to user over the CLI. 
     * @param args Unused. 
     */
    public static void main(String[] args) {

        // DUPLICATING OUTPUT TO LOGGING FILE, IF POSSIBLE
        OutputStream outStream;
        try {
            outStream = new DupingOutputStream(new OutputStream[]
                    {System.out, new FileOutputStream(LOGGING_FILE)});
            PrintStream multiOut = new PrintStream(outStream);
            System.setOut(multiOut);

            System.out.format("\n" +
                            "Info: Using %s as a logging file, " +
                            "all console output is copied there.\n" +
                            "\n", LOGGING_FILE);

        } catch (FileNotFoundException e) {
            System.setOut(System.out);
            System.out.format("Warning: Could not create/open logging file (%s)\n", LOGGING_FILE);
        }
        

        // GENERATING AREA MODEL
        Controller control = null;
        AreaModel model = null;
    	
        try {

            System.out.print("Generating area model... ");
        	Dotter dotPrinter = new Dotter(0, 250).start();
            {
                model = AreaModel.generateFromFile(new BufferedReader(new FileReader(DATA_FILE)));
                control = new Controller(model, AreaModel.get(PRAGUE_FILE_ID));
            }
            dotPrinter.stop();
            System.out.println("Generation successful.");

            System.out.println("There are " + AreaModel.getCityMap().size() + " cities and " + AreaModel.getConnections() + " roads in the model.");
			
		} catch (Exception e) {
            System.out.println("Area model generation was unsuccessful.");
			e.printStackTrace();
			System.exit(1);
		}
        
        
      //control.pendingOrders = model.generateOrders(numOfOrders);
        if(args.length > 0 && Integer.parseInt(args[0]) >= 0 && Integer.parseInt(args[0]) < 2000) {
        	control.setKnownMorningOrderCount(Integer.parseInt(args[0]));
        	System.out.println("Order count set to " + Integer.parseInt(args[0]) + ", continuing...");
        }
        else {
        	System.out.println("Wrong parameter, exiting now.");
        	System.exit(1);
        }

        
        //
        // finding lengths and speeds of paths between each pair of cities
        System.out.print("\nCaching distances between cities...");
        {
            Dotter dotPrinter = new Dotter(0, 500).start();
            FloydWarshall fl = new FloydWarshall();

            fl.floydWarshall();
            allDistances = fl.result.get(0);
            allSpeeds = fl.result.get(1);
            dotPrinter.stop();
        }
        System.out.println("\nCaching completed.");
        
        
        
        
        
        /// ------------------------------
        
        // Give user control over the simulation with the CLI
        new CliControl(control, System.in);
        
    }
    
}

