package simulation;
import java.util.*;

/**
 * Takes care of progressive dot printing 
 * for the implication of longer process (calculation)
 */
class Dotter {

    /** A delay before the dots start being printed */
    private final int delayMs;
    /** The interval between printing of individiual dots */
    private final int intervalMs;
    /** The timer for handling timed print calls */
    private Timer t;

    /**
     * Constructor sets delay and interval of dot printin 
     * @param delayMs delay of dot printing
     * @param intervalMs interval between dots are printed
     */
    Dotter(int delayMs, int intervalMs) {
        this.delayMs = delayMs;
        this.intervalMs = intervalMs;
    }

    /**
     * Starts printing dots to the output. 
     * @return  Dotter instance 
     */
    Dotter start() {
        t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                System.out.print(". ");
            }
        }, delayMs, intervalMs);
        return this;
    }

    /**
     * Stops printing dots to the output
     * @return  Dotter instance
     */
    Dotter stop() {
        System.out.print("\n");
        t.cancel();
        t.purge();
        t = null;
        return this;
    }
}

