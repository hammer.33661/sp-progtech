package simulation;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import java.util.*;

import data.AreaModel;
import structure.Assignment;
import structure.Car;
import structure.City;
import structure.Order;

/**
 * Instance of this class takes care of controlling the simulation flow. 
 */
public class Controller {

	/** List of cars to be sent out on their way to resolve assigned orders */
	ArrayList<Car> carsToBeSent = new ArrayList<>();

	/** Departure time from previous stop */
	double timeOdjezduZPredchoziZastavky; // může být i čas odjezdu ze stanice

	private int numOfCarsInGarage = Integer.MAX_VALUE;

	/// PRINTING FLAGS - CHANGED BY CLI COMMANDS ///
	public boolean PRINT_ORDER_NEW          = true;
	public boolean PRINT_ORDER_NEW_MORNING  = true;
	public boolean PRINT_ORDER_RESOLVED     = true;
	public boolean PRINT_ORDER_ERROR        = true;
	public boolean PRINT_CAR_INITIATED_PATH = true;
	public boolean PRINT_CAR_PROGRESS       = true;
	public boolean PRINT_CAR_UNLOADING      = true;
	public boolean PRINT_CAR_DONE           = true;
	public boolean PRINT_STEP_TIME          = true;
	public boolean PRINT_DAY_SUMMARY        = true;
	/// ///

	/** Time when orders start coming */
	private final SimulationTime INCOMING_ORDERS_START = new SimulationTime(6, 00);
	private final SimulationTime INCOMING_ORDERS_END = new SimulationTime(16, 00);
	private final SimulationTime DAY_SUMMARY_TIME = new SimulationTime(23,30);

	/// SIMULATION TIMING ///

	/** An initial delay of the timer */
	private static final int INITIAL_DELAY_MS = 300;
	/** Initial simulation time */
	private static final SimulationTime INITIAL_TIME = new SimulationTime(5, 30);

	/** Flag for pausing */
	private boolean paused = true;
	/** Flag for enabling morning trigger */
	private boolean newDay = true;

	/** Current simulation time in minutes */
	public SimulationTime time;
	/** Length of one step in real time, in ms */
	int stepRealtimeMillis = 500;
	/** Length of one step in simulation time, in minutes */
	int stepSimulationMinutes = 15;

	/** Timer for executing simulation steps */
	private Timer timer;

	/** Average minutes between order arrival delays */
	private int T = 5;
	/** How many orders are known at the start of the day (the X in assignment) */
	private int knownMorningOrderCount = 100;
	/** Delay between order arrivals, random based on T */
	private int betweenOrderDelay;
	/** Counter for ensuring delay between order arrivals */
	private int countUntilDelay;

	/// SIMULATION MODEL ///

	/** Controlled area model */
	AreaModel model;
	/** Station sending out cars with pallets */
	City station;

	/** Currently pending orders */
	ArrayList<Order> pendingOrders;
	/** Sent cars with assigned orders */
	ArrayList<Car> sentCars;
	/** Cars pending for purging */
	private LinkedList<Car> pendingPurgeCars;

	/// MODEL EVENTS ///

	/** A list of events that took place in the model, with timestamps */
	public SimulationEvents events;
	
	boolean isMorningOrders = false;
	/// SUMMARY COUNTERS ///
    
    /** Number of today's completed orders */
    public int _completedOrders;
    /** Number of today's accepted orders */
    int _acceptedOrders;
    /** Number of today's delivered pallets */
    public int _deliveredPallets;

	/**
	 * @return the knownMorningOrderCount
	 */
	public int getKnownMorningOrderCount() {
		return knownMorningOrderCount;
	}

	/**
	 * @param knownMorningOrderCount the knownMorningOrderCount to set
	 */
	public void setKnownMorningOrderCount(int knownMorningOrderCount) {
		this.knownMorningOrderCount = knownMorningOrderCount;
	}

	/**
	 * Creates a new controller controlling a model of a city area. The instance
	 * controls the simulation in time.
	 *
	 * @param model
	 *            Model of city area
	 * @param station
	 *            Station city in the area
	 *
	 * @throws IllegalArgumentException
	 *             If station city isn't in the area model
	 */
	public Controller(AreaModel model, City station) throws IllegalArgumentException {

		if (model == null)
			throw new IllegalArgumentException();
		if (station == null)
			throw new IllegalArgumentException();

		if (!model.contains(station))
			throw new IllegalArgumentException("Controller error: Station is not a city in area model.");

		String timerName = "sim-timer";

		this.timer = new Timer(timerName);
		this.time = new SimulationTime(INITIAL_TIME);

		this.model = model;
		this.station = station;
		this.pendingOrders = new ArrayList<>();
		this.sentCars = new ArrayList<>();
		this.pendingPurgeCars = new LinkedList<>();

		this.events = new SimulationEvents();
	}

	/// CAR control ///

	/**
	 * Sends out a car with a set number of pallets and it will follow a defined
	 * path to complete its orders.
	 *
	 * @param nPallets
	 *            Number of pallets
	 * @param assignedOrders
	 *            Orders assigned to the car for resolving
	 * @param path
	 *            Path the car will take to resolve the orders
	 * @param departureTimes Car's departure from cities where it unloads any pallets
	 */
	public void sendNewCar(int nPallets, LinkedList<Order> assignedOrders, LinkedList<City> path,
			LinkedList<SimulationTime> departureTimes) {

		// #TODO implement path invalidity checking ??
		// if (pathIsInvalid(path, this.cityMap))
		// throw new IllegalArgumentException("Error sending car:\n Path through the
		// city is invalid.");

		Car newCar = new Car(this, nPallets, assignedOrders, path, departureTimes);
		sentCars.add(newCar);

		event: {
			StringBuilder listedPath = new StringBuilder("");
			for (City city : path) {
				listedPath.append("    -> ");
				listedPath.append(city.getName());
				listedPath.append("\n");
			}

			StringBuilder listedOrders = new StringBuilder("");
			for (Order order : assignedOrders) {
				listedOrders.append("    - ");
				listedOrders.append(order);
				listedOrders.append("\n");
			}

			events.add(time, SimulationEvents.Type.CAR_INITIATED_PATH,
					String.format("" + "New car #%s will resolve %d orders\n" + "%s"
							+ "  taking path through %d cities\n" + "%s", newCar, assignedOrders.size(), listedOrders,
							path.size(), listedPath));
		}
	}

	/**
	 * Adds the car to a list of cars to be removed. On the next {@link #step(int)},
	 * the cars in the list get removed from the {@link #sentCars} list. This
	 * buffering is done to avoid {@link ConcurrentModificationException} during
	 * removal.
	 * 
	 * @param car
	 *            Car to be removed.
	 */
	void purge(Car car) {
		// Flag car for purging on next step()
		pendingPurgeCars.add(car);

		// Pass unresolved assigned orders back to pending
		if (!car.getAssignedOrders().isEmpty())
			pendingOrders.addAll(car.getAssignedOrders());

		event: {
			String description;
			if (car.getAssignedOrders().size() <= 0) {
				description = String.format("" +
                        "Car #%s returned to station %s.", 
                        car, car.getCurrentPosition().getOnRoadTo().getName());
			} else {
				description = String.format("" +
                        "Car #%s returned to station %s with %d remaining orders", 
                        car, car.getCurrentPosition().getOnRoadTo().getName(), car.getAssignedOrders().size());
			}

			events.add(time, SimulationEvents.Type.CAR_DONE, description);
			
		}
	}

	/// SIMULATION FLOW ///

	/**
	 * Starts the simulation loop.
	 */
	void startSimulation() {

		// Timer that calls step on interval
		timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				step(stepSimulationMinutes);
			}
		}, INITIAL_DELAY_MS, stepRealtimeMillis);
		paused = false;
	}

	/**
	 * Executes one single step of simulation, i.e. advances the simulation by
	 * {@code stepSimulationMinutes}.
	 * 
	 * @param stepSimulationMinutes
	 *            Number of minutes to advance the simulation by
	 */
	void step(int stepSimulationMinutes) {

		int numberOfNewOrders = addNewOrders2Pending(stepSimulationMinutes);

		_acceptedOrders += numberOfNewOrders;
		isMorningOrders = time.getHrs() == INCOMING_ORDERS_START.getHrs() && time.getMins() == stepSimulationMinutes;

		//
		// checking whether we resolved anything at all,
		// if not, we stop triyng atm - cycling
		int pred = -1;
		int po = -1;
		int rozdil = -1;
		
		//
		// Sending out cars, then deleting
		// resolved orders from table and from
		// pending orders
		while (!pendingOrders.isEmpty() && rozdil != 0 && numOfCarsInGarage > 0) {
			pred = pendingOrders.size();
			sendCars();
			po = pendingOrders.size();
			rozdil = pred - po;
			if (rozdil == 0) {
				events.add(time, SimulationEvents.Type.ORDER_ERROR, String.format("" +
                        "Left %d orders in pending, they cannot be currently resolved", 
                        pendingOrders.size())
                    );
			}
		}
		if (!pendingOrders.isEmpty() && numOfCarsInGarage < 1) {
			events.add(time, SimulationEvents.Type.ORDER_ERROR, String.format("" +
                    "No cars currently remaining to resolve %d pending orders", 
                    pendingOrders.size())
            );
		}

		
		//
		// figuring out the size of our garage;
		// No. of cars is how many cars we need
		// for resolving morning orders
		if(isMorningOrders) {
			numOfCarsInGarage = carsToBeSent.size();
			isMorningOrders = false;
		}

		
		
		// === SENDING CARS OUT OF STATION ===
		
		int i = 0;
		while(i < carsToBeSent.size()) {

			/*double depTime = this.carsToBeSent.get(i).departureTimes.get(0).hrs
					+ (this.carsToBeSent.get(i).departureTimes.get(0).mins / 60);
			double now = this.time.hrs + (this.time.mins / 60);*/
			
			SimulationTime depTime = this.carsToBeSent.get(i).getDepartureTimes().get(0);
			SimulationTime now = this.time;

			if (depTime.collidesWith(now, stepSimulationMinutes) 
					|| Math.abs(depTime.getMins() - now.getMins()) <= 10/*0.15 >= Math.abs(depTime - now)*/) {
				this.sendNewCar(this.carsToBeSent.get(i).getnLoadedPallets(), this.carsToBeSent.get(i).getAssignedOrders(),
						this.carsToBeSent.get(i).getPlannedPath(), this.carsToBeSent.get(i).getDepartureTimes());
				this.carsToBeSent.remove(i);
				this.carsToBeSent.trimToSize();
			}
			i++;
		}
		
		// ========
		
		
		// Update all sent cars
		for (Car car : sentCars) {
			if (car.isReadyForPurging()) // avoid having to call Controller.purge(this) from car
				purge(car);
			else
				car.step(stepSimulationMinutes);
		}

		// Purge cars if pending for purging
		for (Car purged : pendingPurgeCars) {
			sentCars.remove(purged);
			numOfCarsInGarage++;
		}
		pendingPurgeCars.clear();
		

		///  DAY SUMMARY  ///
        if (time.collidesWith(DAY_SUMMARY_TIME, stepSimulationMinutes)) {
            events.add(time, SimulationEvents.Type.DAY_SUMMARY, String.format("" +
                    "======  SUMMARY OF DAY %d  ======\n" +
                    "    - %d orders accepted \n" +
                    "    - %d orders completed \n" +
                    "    - %d pallets delivered \n" +
                    // "    - %d kilometers driven \n" +
                    // "    - %d minutes on road spent by cars \n" +
                    // "    \n" +
                    // "    Total cost of the day's orders: %d Kč \n"+ 
                    "  ======  END OF SUMMARY  ======", 
                    time.getDay(), 
                    _acceptedOrders, 
                    _completedOrders, 
                    _deliveredPallets)
            );
            _acceptedOrders = 0;
            _completedOrders = 0;
            _deliveredPallets = 0;
        }
		
        ///  EVENT PRINTING  ///
        if (PRINT_STEP_TIME)
            System.out.println("\n\n---- [" + time + "] ----\n\n");
        
        printEvents();
        events.flushEventList();

	    time.addMins(stepSimulationMinutes);
	}

	/**
	 * Resumes the simulation loop.
	 */
	void resumeSimulation() {
		paused = false;
		startSimulation();
	}

	/**
	 * Pauses the simulation loop.
	 */
	void pauseSimulation() {
		paused = true;
		if (timer != null)
			timer.cancel();
	}

	/**
	 * Returns true if the simulation is paused.
	 * 
	 * @return Is the simulation paused?
	 */
	boolean isPaused() {
		return paused;
	}

	/// ORDER FLOW ///

	/**
	 * Adds orders to {@link #pendingOrders}.
	 * 
	 * If the {@link #time} just went over {@link #INCOMING_ORDERS_START} (i.e.
	 * morning hour), it will add {@link #knownMorningOrderCount} of orders. These
	 * are the orders, that are known in advance.
	 * 
	 * If the time is between {@link #INCOMING_ORDERS_START} and
	 * {@link #INCOMING_ORDERS_END}, the function randomly generates new orders to
	 * be put into {@link #pendingOrders}.
	 * 
	 * If it's not time for incoming orders, no orders are added.
	 * 
	 * @param stepSimulationMinutes
	 *            Minutes passed in simulation minutes.
	 * @return Number of orders added to {@link #pendingOrders}.
	 */
	private int addNewOrders2Pending(int stepSimulationMinutes) {

		// Advance delay counter
		countUntilDelay += stepSimulationMinutes;

		// If we haven't reached delay with counter, end fn
		if (countUntilDelay < betweenOrderDelay)
			return 0;

		// Apply current time to remaining step sim. minutes
		stepSimulationMinutes -= (betweenOrderDelay - countUntilDelay);
		countUntilDelay = 0;

		// Number of added orders
		int nOrders = 0;

		// Relevant time flags
		boolean isAfterMorning = (time.getDayMins() > INCOMING_ORDERS_START.getDayMins());
		boolean isBeforeNight = (time.getDayMins() < INCOMING_ORDERS_END.getDayMins());
		boolean isOrderTime = (isAfterMorning && isBeforeNight);
		boolean isMorning = false;
		if (newDay && isAfterMorning) {
			isMorning = true;
			newDay = false;
		}

		// If it's not order-accepting time, end fn
		if (!isOrderTime)
			return 0;
		else {

			// Known morning orders
			if (isMorning) {

				StringBuilder morningOrders = new StringBuilder("");

				for (int i = 0; i < knownMorningOrderCount; i++) {
					Order randOrder = Order.random(model);
					pendingOrders.add(randOrder);

					morningOrders.append("\n   - ");
					morningOrders.append(randOrder);
				}
				nOrders = knownMorningOrderCount;

				events.add(time, SimulationEvents.Type.ORDER_NEW_MORNING,
						"=== MORNING ORDERS ===" + "" + morningOrders + "\n" + "  ========= END =========\n");

				return nOrders;
			}

			// Orders incoming during the day
			do {
				// Add new order
				Order newOrder = Order.random(model);
				pendingOrders.add(newOrder);
				nOrders++;

				event: {
					events.add(time, SimulationEvents.Type.ORDER_NEW, "" + newOrder);
				}

				// Create a delay until a next order appears
				betweenOrderDelay = randomExponentialDelay(this.T);
				stepSimulationMinutes -= betweenOrderDelay;
				// System.out.println("Next order in "+ betweenOrderDelay + " minutes");

			} while (stepSimulationMinutes > betweenOrderDelay);

			return nOrders;
		}
	}

	/**
	 * Returns a random delay between orders with an expected value of {@code mean}.
	 * 
	 * @param mean
	 *            The mean/average/expected value of the random function
	 * @return A random delay between orders, in minutes
	 * 
	 * @see <a href=
	 *      'http://www2.ef.jcu.cz/~rost/courses/stata/data/exponencialni%20rozdeleni.pdf'>
	 *      Exponential probability distribution</a>
	 * @see <a href=
	 *      'https://stackoverflow.com/questions/2106503/pseudorandom-number-generator-exponential-distribution'>
	 *      Pseudorandom number following the exponential distribution</a>
	 */
	private int randomExponentialDelay(int mean) {
		double rand = Math.random(); // Has to be a uniform random value
		double value = Math.log(1 - rand) * (-mean);
		return (int) Math.round(value);
	}

	/// OTHER ///

	/**
	 * Prints the events from {@link #events} list, sorted by the time the events
	 * happened.
	 */
	private void printEvents() {
		ArrayList<SimulationEvents.Event> sortedEvents = events.getEventsSortedByTime();

		for (SimulationEvents.Event event : sortedEvents) {
			boolean print = false;

			switch (event.getType()) {
                case ORDER_NEW:
                    print = PRINT_ORDER_NEW;
                    break;
                case ORDER_NEW_MORNING:
                    print = PRINT_ORDER_NEW_MORNING;
                    break;
                case ORDER_RESOLVED:
                    print = PRINT_ORDER_RESOLVED;
                    break;
                case ORDER_ERROR:
                    print = PRINT_ORDER_ERROR;
                    break;
                case CAR_INITIATED_PATH:
                    print = PRINT_CAR_INITIATED_PATH;
                    break;
                case CAR_PROGRESS:
                    print = PRINT_CAR_PROGRESS;
                    break;
                case CAR_UNLOADING:
                    print = PRINT_CAR_UNLOADING;
                    break;
                case CAR_DONE:
                    print = PRINT_CAR_DONE;
                    break;
                case DAY_SUMMARY: 
                    print = PRINT_DAY_SUMMARY;
                    break;
				
                default: 
                    print = true;
                    break;
			} // #TODO event types block could be simplified

			if (print)
				System.out.println(event);
		}
	}

	/**
	 * Sends out cars to resolve orders. Searches for a new Order for a Car to
	 * resolve as long as it's possible to find one. Once the algorithm decides
	 * there's no other Order to be added to the same car, it adds a new Car to a
	 * {@code carsToBeSent} list.
	 */
	void sendCars() {
		boolean hledejDal = true;
		boolean isFirstStop = true;

		Assignment assignment = new Assignment();

		while (hledejDal) {
			hledejDal = najdiDalsiZastavku(isFirstStop, assignment);
			isFirstStop = false;

		}

		if (assignment.getnLoadedPallets() > 0) {
			assignment.getPlannedPath().addAll(model.getShortestPathBetween(assignment.getPlannedPath().getLast(), this.station));
			Car auto = new Car(this, assignment.getnLoadedPallets(), assignment.getAssignedOrders(), assignment.getPlannedPath(),
					assignment.getDepartureTimes());
			carsToBeSent.add(auto);
			if (numOfCarsInGarage > 0) {
				numOfCarsInGarage--;
			}

		} else {
			assignment = null;
		}

	}

	/**
	 * Algorithm for finding Orders for cars and calculating their departure times
	 * 
	 * Finds the best resolvable Order - an order that we accept must fit into this
	 * condition: - we must be able to load additional pallets onto the Car - we
	 * must make it to closing time - or we must arrive exactly at opening hour and
	 * have more than 3 pallets loaded
	 * 
	 * Tries to find additional Orders which we can load onto the same Car.
	 * 
	 * It calculates all departure times for Cars
	 * 
	 * @param isFirstStop
	 *            true/false if we're searching for the first stop (departuring from
	 *            {@code control.station})
	 * @param assignment
	 *            holds all important info for a new Car
	 * @return true if another Order was found and 'resolved'
	 */
	boolean najdiDalsiZastavku(boolean isFirstStop, Assignment assignment) {
		double dobaCesty;
		double casVyreseniObjednavky = Double.POSITIVE_INFINITY;
		double tempCasVyreseniObjednavky;
		int indexNejvhodnejsiDalsi = -1;
		double casPrijezdu;
		double dobaVykladky;

		int indexPrvni;
		int indexDruhy;

		SimulationTime casOdjezduAuta = null;

		City posledniMesto;

		if (isFirstStop) {
			timeOdjezduZPredchoziZastavky = this.time.getHrs() + (this.time.getMins() / 60);
			posledniMesto = this.station;

		} else {
			posledniMesto = assignment.getPlannedPath().getLast();
		}

		int i = 0;

		while (i < pendingOrders.size()) {

			indexPrvni = posledniMesto.getId();
			indexDruhy = pendingOrders.get(i).getTarget().getId();

			dobaCesty = Program.getAllDistances()[indexPrvni][indexDruhy]
					/ (double) (Program.getAllSpeeds()[indexPrvni][indexDruhy]);

			casPrijezdu = timeOdjezduZPredchoziZastavky + dobaCesty;
			dobaVykladky = 0.5 * pendingOrders.get(i).getRequiredPallets();
			tempCasVyreseniObjednavky = casPrijezdu + dobaVykladky;

			//
			// we must be able to load additional pallets onto the Car
			// we must make it to closing time
			// or we must arrive exactly at opening hour and have more than 3 pallets loaded
			if ((assignment.getnLoadedPallets() + pendingOrders.get(i).getRequiredPallets() <= Car.MAX_LOADED_PALLETS)
					&& ((casPrijezdu + dobaVykladky <= pendingOrders.get(i).getTarget().getClosingHour())
							|| (casPrijezdu <= pendingOrders.get(i).getTarget().getOpeningHour()
									&& pendingOrders.get(i).getRequiredPallets() > 3)))

			{

				//
				// if we found something that suits these conditions, we check wether
				// we can resolve this order the quickest
				// we set the departure time for "last minute" the car can make it
				if (tempCasVyreseniObjednavky < casVyreseniObjednavky) {
					casVyreseniObjednavky = tempCasVyreseniObjednavky;
					indexNejvhodnejsiDalsi = i;

					double hodiny = Math.floor(pendingOrders.get(i).getTarget().getOpeningHour() - dobaCesty);
					double minuty = ((pendingOrders.get(i).getTarget().getOpeningHour() - dobaCesty) % 1) * 60;
					casOdjezduAuta = new SimulationTime((int) hodiny, (int) minuty);
				}
			}

			i++;
		}

		//
		// once we got the best order to resolve atm, we save important info
		// for the Car in zakazka and we save departure time from this current Order
		// (previous order for future Orders)
		if (indexNejvhodnejsiDalsi > -1) {
			assignment.setnLoadedPallets(assignment.getnLoadedPallets() + pendingOrders.get(indexNejvhodnejsiDalsi).getRequiredPallets());
			assignment.getAssignedOrders().add(pendingOrders.get(indexNejvhodnejsiDalsi));
			assignment.getPlannedPath().addAll(
					model.getShortestPathBetween(posledniMesto, pendingOrders.get(indexNejvhodnejsiDalsi).getTarget()));
			if (isFirstStop) {
				assignment.getPlannedPath().addFirst(this.station);
			}
			assignment.getDepartureTimes().add(casOdjezduAuta);

			if (indexNejvhodnejsiDalsi < pendingOrders.size()) {
				pendingOrders.remove(indexNejvhodnejsiDalsi);
				pendingOrders.trimToSize();
			} else {
				System.out.println(" Something went wrong in car sending...");
			}

			timeOdjezduZPredchoziZastavky = casOdjezduAuta.getHrs() + (casOdjezduAuta.getMins() / 60);

			return true;
		} else {
			return false;
		}
	}

}
