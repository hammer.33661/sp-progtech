package simulation;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

import data.AreaModel;
import structure.Car;
import structure.Order;


/**
 * The {@code CliControl} class allows the user to control the
 * simulation in a shell-like environment via text commands. 
 */
public class CliControl {

    /** An instance of a simulation controller */
    private Controller control;
    /** A list of available commands */
    private ArrayList<Command> commands;
    /** Scanner of the input stream given in constructor */
    private Scanner scan;

    /** Name of the 'help' command */
    private static final String HELP_CMDNAME = "help";
    /** Prefix for command-specific help */
    private static final String CMDHELP_USAGE_PREFIX = "help ";

    /**
     * Gives the control over the controller to the user via the given
     * {@code InputStream inStream}. This enables the user
     * to use a shell-like interface to communicate with the simulation
     * program and change the program's parameters mid-simulation. 
     * 
     * @param control   Simulation controller
     * @param inStream  Input stream to use for control
     */
    public CliControl(Controller control, InputStream inStream) {
        this.control = control;
        this.scan = new Scanner(inStream);
        this.commands = generateCommands();

        giveUserCLIcontrol();
    }

    /**
     * Initiates the control of the controller. This method includes 
     * the main query loop, ended by an `exit' command. 
     */
    private void giveUserCLIcontrol() {

        System.out.format("\n\n" +
                "You can now use text commands. \n" +
                "\n" +
                "Press Enter to start simulation or input 'help' \n" +
                "to list your options. \n" +
                "\n" +
                "Pressing Enter while the simulation is running \n" +
                "will pause it. \n");

        control.pauseSimulation();

        while (true) {  // Loop ends when user enters "exit" or "quit" into the console
        	
            // Wait for user pressing Enter
            System.out.print("\n> ");
            String line = scan.nextLine().trim().toLowerCase();
            // Infinite loop ends when user enters "exit" or "quit"

            if (control.isPaused()) {

                // Issue commands

                if (line.isEmpty()) {
                    System.out.println("Resuming simulation...");
                    control.resumeSimulation();
                } else {
                    execute(line);
                }

            } else {

                System.out.println("Simulation paused at [" + control.time + "].");
                System.out.println("Waiting for input...");
                control.pauseSimulation();

            }
          
        }
    }

    /**
     * Generates a list of commands available to the user from 
     * hard-coded command entries. This avoids re-writing 
     * the command help and hard-coding the detection of commands 
     * for every single entry.  
     * 
     * @return  A list of usable commands
     */
    private ArrayList<Command> generateCommands() {

        ArrayList<Command> cmds = new ArrayList<>();
        
        /*   ENTRY TEMPLATE   *
        
        cmds.add(new Command(
                "", new String[]
                {""}, 
                "") {
            
            @Override
            void action() {
                
            }
        });
        
        /**/

        
        
        ///  STATUS QUERIES  ///
        

        cmds.add(new Command(
                "time", new String[]
                {"time", "status time"},
                "Prints the current simulation time of latest step. ") {

            @Override
            void action() {
                System.out.format("Current time: %s\n", control.time);
            }
        });
        
        cmds.add(new Command(
                "status cars", new String[]
                {"cars", "status c"},
                "View all cars' positions. This command prints the names of cars, \n" +
                "their current position and how fast they're travelling. ") {

            @Override
            void action() {
                System.out.println("\nPrinting status of cars in model: ");
                for (Car c : control.sentCars)
                    System.out.format("  %s - %s\n", c, c.getCurrentPosition());
            }
        });

        cmds.add(new Command(
                "status orders", new String[]
                {"status o", "status as"},
                "View orders assigned to cars. These printed orders are currently \n" +
                "being resolved by outgoing cars. Cars returning from resolved \n" +
                "orders are not printed. ") {

            @Override
            void action() {
                System.out.println("\nPrinting status of assigned orders: ");
                for (Car c : control.sentCars)
                    for (Order o : c.getAssignedOrders())
                        System.out.format("  Order %s assigned to %s (%s)\n",
                                o, c, c.getCurrentPosition());
            }
        });

        cmds.add(new Command(
                "status pending", new String[]
                {"status pend", "status station"},
                "View orders pending at the station. Such orders are waiting to be \n" +
                "taken by a car - this order is not currently being resolved. ") {

            @Override
            void action() {
                System.out.println("\n" +
                        "Printing orders currently pending at the station: ");
                for (Order o : control.pendingOrders)
                    System.out.format("  - %s\n", o);
            }
        });

        cmds.add(new Command(
                "status step", new String[]
                {"status st", "status real", "status sim"},
                "View simulation and real time step values. These values indicate \n" +
                "how much simulation time (in minutes) will pass during \n" +
                "one real time step (in milliseconds). ") {

            @Override
            void action() {
                System.out.format("\n" +
                                "Every %d milliseconds in real time, the simulation time \n" +
                                "is incremented by %d minutes. \n",
                        control.stepRealtimeMillis,
                        control.stepSimulationMinutes);
            }
        });

        cmds.add(new Command(
                "status model", new String[]
                {"status mod"},
                "View attributes of model. This will print basic statistics of \n" +
                "the currently simulated model. ") {

            @Override
            void action() {
                String indent = control.model.statistics();
                indent = indent.replaceAll("(?m)^", "  ");
                
                System.out.format("\n" +
                        "Model statistics: \n" +
                        "" + indent + "\n");
            }
        });
        
        
        ///  FILE OUTPUT CONTROL  ///
        
        

        
        ///  CONSOLE OUTPUT CONTROL  ///

        
        
        
        ///  PARAMETER CONTROL  ///

        cmds.add(new Command(
                "set prices", new String[]
                {"set pri", "set cost"},
                "[NOT IMPLEMENTED YET] \n" +
                "Sets the prices of travel per kilometer and per hour. The simulation \n" +
                "uses these prices to optimize cheapest travelling to resolve orders. ") {

            @Override
            void action() {
                System.out.println("[NOT IMPLEMENTED YET]");
            }
        });
        

        ///  SIMULATION CONTROL  ///
        
        cmds.add(new Command(
                "new order", new String[]
                {"new o", "order", "new"}, 
                "Add new order for the firm to resolve") {
            
            @Override
            void action() {
            	
            	System.out.format("\n" +
                        "Enter the city's id and a number \n"
                        + "of pallets to be delivered. \n"
                        + "\n"
                        + "[city id,number of pallets] > ");
            	
            	String line = scan.nextLine();
            	if (line != null && !(line.equals("")) && line.contains(",")) {
                    String[] split = line.split(",");
                    
                    int cityID = Integer.parseInt(split[0]);
                    int pal = Integer.parseInt(split[1]);
                    
                    if (pal > 0 && pal < 6 && cityID >= 0 && cityID < 2000) {
                        System.out.format("Adding new order of %d pallets for city %s...\n", Integer.parseInt(split[1]), split[0]);
                        control.pendingOrders.add(new Order(AreaModel.get(cityID), pal));
                        return;
                    } else {
                        System.out.format("Wrong input, added no new order. Resume simulation and try again. \n");
                    }
                }
            	
                if (!line.isEmpty()) System.out.format("Wrong input. ");
                System.out.format("No order was added.");
                
            }
        });
        
        cmds.add(new Command(
                "set sim step", new String[]
                {"set sim st", "set st", "set simulation st"},
                "Change simulation step (in simulation minutes). ") {

            @Override
            void action() {

                System.out.format("\n" +
                        "Simulation step interval currently set to: " +
                        "%d minutes\n", control.stepSimulationMinutes);

                System.out.format("\n" +
                        "Input a number of minutes between every simulated\n" +
                        "step of the simulation. \n" +
                        "Or input an empty line to leave existing value. \n" +
                        "\n" +
                        "[minutes] > ");

                if (scan.hasNextInt()) {
                    String cleanLine = scan.nextLine().replaceAll("[^\\d]", "");
                    int newval = Integer.valueOf(cleanLine);
                    if (newval > 0) {
                        System.out.format("Setting simulation step to %d...\n", newval);
                        control.stepSimulationMinutes = newval;
                        return;
                    } else {
                        System.out.format("Number of minutes must be non-zero positive. \n");
                    }
                }

                String line = scan.nextLine(); // Dump this invalid line
                if (!line.isEmpty()) System.out.format("This is not a number. ");
                System.out.format("Leaving default value of: %d minutes.\n",
                        control.stepSimulationMinutes);
            }
        });

        cmds.add(new Command(
                "set real step", new String[]
                {"set real st", "set realtime st"},
                "Change real time step (in milliseconds). ") {

            @Override
            void action() {

                System.out.format("\n" +
                        "Realtime step interval currently set to: " +
                        "%d ms\n", control.stepRealtimeMillis);

                System.out.format("\n" +
                        "Input a number of milliseconds between every simulated\n" +
                        "step of the simulation. \n" +
                        "Or input an empty line to leave existing value. \n" +
                        "\n" +
                        "[ms] > ");

                if (scan.hasNextInt()) {
                    String cleanLine = scan.nextLine().replaceAll("[^\\d]", "");
                    int newval = Integer.valueOf(cleanLine);
                    if (newval > 0) {
                        System.out.format("Setting realtime step to %d...\n", newval);
                        control.stepRealtimeMillis = newval;
                        return;
                    } else {
                        System.out.format("Number of milliseconds must be non-zero positive. \n");
                    }
                }

                String line = scan.nextLine(); // Dump this invalid line
                if (!line.isEmpty()) System.out.format("This is not a number. ");
                System.out.format("Leaving default value of: %d ms.\n",
                        control.stepRealtimeMillis);
            }
        });

        
        ///  EXIT AND HELP COMMANDS AT THE END  ///

        cmds.add(new Command(
                "exit", new String[]
                {"ex", "q"},
                "Exits out of the program. ") {

            @Override
            void action() {
                System.out.format("\n" +
                        "Exiting program.\n");
                System.exit(0);
            }
        });

        cmds.add(new Command(
                HELP_CMDNAME, new String[]
                {"h", "?"},
                "Prints help for controlling this simulation.") {

            @Override
            void action() {
                String helpBlock = "\n" +
                        "When paused, you can make changes to the model \n" +
                        "and view attributes of the simulation objects. \n" +
                        "\n" +
                        "List of all commands: \n" +
                        "" + generateCommandHelp() +
                        "\n" +
                        "There will be other commands coming. \n" +
                        "\n" +
                        "Input command or press enter (input empty line) to resume simulation. ";
                System.out.println(helpBlock);
            }
        });

        return cmds;
    }

    /**
     * Returns a command that alignes with the given {@code commandName}. 
     * This method looks at the abbreviations for all commands
     * and checks if the command starts with some command's abbreviation. 
     * 
     * @param commandName  Name of the searched command
     * @return  Command which commandName starts with 
     */
    private Command findCommand(String commandName) {

        for (Command cmd : commands)
            for (String alias : cmd.abbrev)
                if (commandName.startsWith(alias))
                    return cmd;
        return null;
    }

    /**
     * Prints a command-specific help -- a description. 
     * @param commandName  Name of the searched command
     */
    private void descriptionOf(String commandName) {

        if (commandName.trim().length() == 0) return;

        Command cmd = findCommand(commandName);
        if (cmd == null) {
            System.out.format("'%s' is an unknown command.\n", commandName);
            return;
        }

        System.out.format("'%s': %s\n", cmd.name, cmd.description);
    }

    /**
     * Executes the command's action method, but if the {@code commandName} 
     * starts with the command-specific help prefix, it prints the command's
     * description. 
     * 
     * @param commandName  Name of the searched command
     */
    void execute(String commandName) {

        if (commandName.trim().length() == 0) return;

        if (commandName.startsWith(CMDHELP_USAGE_PREFIX)) {

            commandName = commandName.substring(CMDHELP_USAGE_PREFIX.length());
            descriptionOf(commandName);

        } else {

            Command cmd = findCommand(commandName);
            if (cmd == null) {
                System.out.format("'%s' is an unknown command, cannot execute it.\n", commandName);
                return;
            }
            cmd.action();
        }

    }

    /**
     * Generates a text block with names and descriptions 
     * of all usable commands. 
     * 
     * @return  Command help text block. 
     */
    private String generateCommandHelp() {
        StringBuilder sb = new StringBuilder("");
        for (Command cmd : commands) {
            // sb.append(String.format(" %-16s  %s\n", "'"+ cmd.name +"':\n" + "   ", cmd.description));
            
            // Command name
            String command = "'"+cmd.name+"': \n";
            sb.append(command);
            
            // Command description in a 54-character block
            String description = cmd.description.replaceAll("(?m)^", "  ");
            sb.append(description);
            sb.append("\n\n");
        }
        return sb.toString();
    }


    /**
     * An abstract class for an executable command. 
     */
    abstract class Command {
        
        /** Basic name of the command, will be listed in help. */
        final String name;
        /** List of possible abbreviations for this command.  */
        final String[] abbrev;
        /** Description of the command's action method. */
        final String description;

        /**
         * An executable command. 
         * The {@code name} will be listed in help as the basic
         * form of the command. 
         * Abbreviations are used for command detection. If the command
         * starts with a command's abbreviation, that command is used. 
         * The description of the command is a text block. If a line
         * is too long (approx. 60 characters), it is recommended
         * to use a `\n' character to wrap the line -- line wrapping
         * is not handled by this object. 
         * 
         * @param name        Basic name of the command
         * @param abbrev      List of abbreviations of the command
         * @param description Description of the command's action method
         */
        Command(String name, String[] abbrev, String description) {
            this.name = name;
            this.abbrev = abbrev;
            this.description = description;
        }

        /**
         * Method that is executed by this command. 
         */
        abstract void action();
    }
}
