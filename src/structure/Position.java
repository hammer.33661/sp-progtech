package structure;
/**
 * The instances of this class represent a position 
 * of an object (mainly the {@link Car}s) in the model. 
 */
public class Position {

    /** City the car departs from */
    private City fromCity;
    /** City the car is heading to */
    private City onRoadTo;

    /** How fast the car is going (in km/h) */
    private int speed;
    /** How far from {@code fromCity} the car is (in km) */
    private float distanceRemaining;

    /** Time required to unload current pallets in order city */
    private int minutesLeft2Unload;



    /**
	 * @return the fromCity
	 */
	public City getFromCity() {
		return fromCity;
	}



	/**
	 * @param fromCity the fromCity to set
	 */
	public void setFromCity(City fromCity) {
		this.fromCity = fromCity;
	}



	/**
	 * @return the onRoadTo
	 */
	public City getOnRoadTo() {
		return onRoadTo;
	}



	/**
	 * @param onRoadTo the onRoadTo to set
	 */
	public void setOnRoadTo(City onRoadTo) {
		this.onRoadTo = onRoadTo;
	}



	/**
	 * @return the speed
	 */
	public int getSpeed() {
		return speed;
	}



	/**
	 * @param speed the speed to set
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
	}



	/**
	 * @return the distanceRemaining
	 */
	public float getDistanceRemaining() {
		return distanceRemaining;
	}



	/**
	 * @param distanceRemaining the distanceRemaining to set
	 */
	public void setDistanceRemaining(float distanceRemaining) {
		this.distanceRemaining = distanceRemaining;
	}



	/**
	 * @return the minutesLeft2Unload
	 */
	public int getMinutesLeft2Unload() {
		return minutesLeft2Unload;
	}



	/**
	 * @param minutesLeft2Unload the minutesLeft2Unload to set
	 */
	public void setMinutesLeft2Unload(int minutesLeft2Unload) {
		this.minutesLeft2Unload = minutesLeft2Unload;
	}



	@Override
    public String toString() {

        if (distanceRemaining == 0) {
            return fromCity.getName();

        } else {
            return String.format(
                    "%-14s -> %14s  (%6.2f km @ %3d km/h)",
                    fromCity.getName(), onRoadTo.getName(), distanceRemaining, speed);
        }
    }
}
