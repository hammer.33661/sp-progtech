package structure;

import data.AreaModel;

/**
 * Order holds the information about an order for a station
 * in the {@link AreaModel}. Information about the closing
 * and opening hour when the order can be resolved is stated
 * in the instance of the {@link #target} {@link City} itself. 
 */
public class Order {

    /** A city ordering pallets */
    private City target;
    /** How many pallets is the city ordering */
    private int requiredPallets;

    /**
	 * @param target the target to set
	 */
	public void setTarget(City target) {
		this.target = target;
	}

	/**
	 * @param requiredPallets the requiredPallets to set
	 */
	public void setRequiredPallets(int requiredPallets) {
		this.requiredPallets = requiredPallets;
	}

	/**
     * An instance of the {@code Order} class is an order
     * existing in an {@link AreaModel}. This order consists
     * of a {@link #target} city which is ordering a number
     * of {@link #requiredPallets}. 
     * @param target           Who is ordering the pallets
     * @param requiredPallets  How many is the city ordering
     */
    public Order(City target, int requiredPallets) {

        if (requiredPallets > Car.MAX_LOADED_PALLETS)
            // #TODO Split into multiple orders??
            throw new IllegalArgumentException(String.format("" +
                    "Order error:\n" +
                    " One order can require only a car's maximum load of pallets\n" +
                    " (%d required, but %d is max)", 
                    requiredPallets, Car.MAX_LOADED_PALLETS));

        this.target = target;
        this.requiredPallets = requiredPallets;
    }

    /**
     * Getter for the number of pallets required by the {@link #target} 
     * to resolve the order. 
     * @return  How many pallets the {@link #target} city needs 
     */
    public int getRequiredPallets() {
    	return this.requiredPallets;
    }

    /**
     * Getter for the order's {@link #target} city (who's ordering the pallets). 
     * @return  Ordering city
     */
    public City getTarget() {
    	return this.target;
    }

    @Override
    public String toString() {
        return String.format("%d pallets to %s (open %d-%d)", 
                requiredPallets, target.getName(), target.getOpeningHour(), target.getClosingHour());
    }

    /**
     * Generates a random order in the {@code model}. Has built-in chances 
     * for certain pallet values - see the assignment paper for details. 
     * 
     * @param model  Model to choose the random order from. 
     * @return  A random order existing in the {@code model}
     */
    public static Order random(AreaModel model) {
        
        int n = AreaModel.getCityMap().size();
        int randID = (int) (Math.random() * n);
        City city = AreaModel.getCityMap().get(randID);
        
        
        final int PALLET_i = 0;
        final int CHANCE_i = 1;
        int[][] chances = {
                {1, 25}, 
                {2, 25}, 
                {3, 20}, 
                {4, 15}, 
                {5, 10}, 
                {6,  5} 
        };

        int nPallets = 0;
        double roll = Math.random() * 100;
        int percent = 0;
        for (int[] chance : chances) {

            percent += chance[CHANCE_i];

            if (roll < percent) {
                nPallets = chance[PALLET_i];
                break;
            }
        }
        
        
        return new Order(city, nPallets);
        
    }
}
