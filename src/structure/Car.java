package structure;
import java.util.LinkedList;

import data.RoadData;
import simulation.Controller;
import simulation.SimulationEvents;
import simulation.SimulationTime;


/**
 * Cars in the model have the role of carrying pallets between multiple 
 * instances of {@link City} to resolve {@link Order}s.
 */
public class Car {

    /** A maximum number of pallets the car can hold at once */
    public static final int MAX_LOADED_PALLETS = 6;
    
    /** The time duration it takes the car to unload a single pallet in a city */
    static final int MINUTES_TO_UNLOAD_SINGLE_PALLET = 30;

    /** Identifier of this car */
    private String identifier;
    
    /** Controller this car is controller by */
    private Controller controller;

    /** The number of loaded pallets on this car */
    private int nLoadedPallets;

    /** Assigned pending orders resolved by the car's planned path */
    private LinkedList<Order> assignedOrders;

    /** A planned path that resolves assigned pendingOrders, including path back to station */
    private LinkedList<City> plannedPath;

    /** Current position of the car */
    private Position currentPosition;
    
    private LinkedList<SimulationTime> departureTimes;

    /** Flag determining if the car is ready to be purged */
    private boolean readyForPurging = false;

    /**
	 * @return the identifier
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * @param identifier the identifier to set
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	/**
	 * @return the controller
	 */
	public Controller getController() {
		return controller;
	}

	/**
	 * @param controller the controller to set
	 */
	public void setController(Controller controller) {
		this.controller = controller;
	}

	/**
	 * @return the nLoadedPallets
	 */
	public int getnLoadedPallets() {
		return nLoadedPallets;
	}

	/**
	 * @param nLoadedPallets the nLoadedPallets to set
	 */
	public void setnLoadedPallets(int nLoadedPallets) {
		this.nLoadedPallets = nLoadedPallets;
	}

	/**
	 * @return the assignedOrders
	 */
	public LinkedList<Order> getAssignedOrders() {
		return assignedOrders;
	}

	/**
	 * @param assignedOrders the assignedOrders to set
	 */
	public void setAssignedOrders(LinkedList<Order> assignedOrders) {
		this.assignedOrders = assignedOrders;
	}

	/**
	 * @return the plannedPath
	 */
	public LinkedList<City> getPlannedPath() {
		return plannedPath;
	}

	/**
	 * @param plannedPath the plannedPath to set
	 */
	public void setPlannedPath(LinkedList<City> plannedPath) {
		this.plannedPath = plannedPath;
	}

	/**
	 * @return the currentPosition
	 */
	public Position getCurrentPosition() {
		return currentPosition;
	}

	/**
	 * @param currentPosition the currentPosition to set
	 */
	public void setCurrentPosition(Position currentPosition) {
		this.currentPosition = currentPosition;
	}

	/**
	 * @return the departureTimes
	 */
	public LinkedList<SimulationTime> getDepartureTimes() {
		return departureTimes;
	}

	/**
	 * @param departureTimes the departureTimes to set
	 */
	public void setDepartureTimes(LinkedList<SimulationTime> departureTimes) {
		this.departureTimes = departureTimes;
	}

	/**
	 * @return the readyForPurging
	 */
	public boolean isReadyForPurging() {
		return readyForPurging;
	}

	/**
	 * @param readyForPurging the readyForPurging to set
	 */
	public void setReadyForPurging(boolean readyForPurging) {
		this.readyForPurging = readyForPurging;
	}

	/**
     * Creates a new car that will be loaded with a number of pallets
     * to resolve its given {@link Order}s. The exact path (list of cities)
     * the car should take to finish the orders has to be given as well. 
     * 
     * @param controller      A controller that controls this car 
     * @param nLoadedPallets  Number of pallets to be loaded onto the car, see {@link #MAX_LOADED_PALLETS}
     * @param assignedOrders  Orders to be resolved by the planned path
     * @param plannedPath     Planned path resolving assigned orders
     * @param departureTimes  Departure time from each ordering city on it's route     TODO - make 'em wait
     * @param identifier      Identifier of this car
     */
    public Car(
            Controller controller, int nLoadedPallets, 
            LinkedList<Order> assignedOrders, LinkedList<City> plannedPath, 
            LinkedList<SimulationTime> departureTimes, String identifier) {

        if (controller == null || assignedOrders == null || plannedPath == null) 
            throw new IllegalArgumentException();
        
        for (Order o : assignedOrders)
            if (!plannedPath.contains(o.getTarget()))
                throw new IllegalArgumentException("The planned path doesn't resolve assigned pendingOrders");
        if (plannedPath.getFirst() != plannedPath.getLast()) 
            throw new IllegalArgumentException("The planned path doesn't end where it started (station)");

        if (nLoadedPallets < 1 || nLoadedPallets > MAX_LOADED_PALLETS)
            throw new IllegalArgumentException("" +
                    "Error sending car:\n " +
                    "Car has to hold at most " + MAX_LOADED_PALLETS + " pallets ("+ nLoadedPallets + ")");

        if (plannedPath.size() < 2)
            throw new IllegalArgumentException("Error sending car:\n Planned path has to consist of at least 2 cityMap.");

        this.controller = controller;
        this.nLoadedPallets = nLoadedPallets;
        this.assignedOrders = assignedOrders;
        this.plannedPath = plannedPath;
        this.departureTimes = departureTimes;
        this.currentPosition = null;
        
        if (identifier == null) 
            this.identifier = this.toString();
        else 
            this.identifier = identifier;
    }

    /** @see #Car(Controller, int, LinkedList, LinkedList, LinkedList, String) */
    public Car(
            Controller controller, int nLoadedPallets, 
            LinkedList<Order> assignedOrders, LinkedList<City> plannedPath, 
            LinkedList<SimulationTime> departureTimes) {
        this(controller, nLoadedPallets, assignedOrders, plannedPath, departureTimes, null);
    }

    /**
     * Advances the car's progress by an amount of minutes. With this, the car 
     * can resolve orders from {@link #assignedOrders} and it can move from a city
     * to another along its {@link #plannedPath}. 
     * 
     * @param stepSimulationMinutes  Number of minutes to advance in time
     */
    public void step(int stepSimulationMinutes) {

        int currentStepLength = stepSimulationMinutes;

        // If we haven't started yet, initialize first journey segment
        if (currentPosition == null) currentPosition = new Position();
        if (currentPosition.getFromCity() == null) {
            City startingPoint = plannedPath.pollFirst();
            City firstCity     = plannedPath.pollFirst();
            if (startingPoint == null || firstCity == null) {
                readyForPurging = true;
                return;
            }
            advanceToNextSegment(startingPoint, firstCity);
        }
        
        
        controller.events.add(
                new SimulationTime(controller.time), 
                SimulationEvents.Type.CAR_PROGRESS, 
                String.format("#%s: %s", this.toString(), currentPosition)
        );

        
        // If car needs to unload...
        if (currentPosition.getMinutesLeft2Unload() > 0) {
            
            // If car can unload during this time step...
            if (currentPosition.getMinutesLeft2Unload() <= stepSimulationMinutes) {
                
                stepSimulationMinutes -= currentPosition.getMinutesLeft2Unload();
                currentPosition.setMinutesLeft2Unload(0);
                // There's some time left after unloading, continue
                
                controller.events.add(
                        new SimulationTime(
                                controller.time, 
                                +(currentStepLength-stepSimulationMinutes)), 
                        SimulationEvents.Type.ORDER_RESOLVED, 
                        String.format(
                                "Car #%s just finished unloading in %s.",
                                this, this.currentPosition.getFromCity().getName())
                );
                
            // ... else the car has to continue unloading for this whole step! 
            } else {
                
                currentPosition.setMinutesLeft2Unload(currentPosition.getMinutesLeft2Unload() - stepSimulationMinutes);
                // Do nothing but unloading this step
                return;
            }
        } 
        
        
        // How much the car has moved during the simulation minutes
        float carDistancePerStep = currentPosition.getSpeed() * (stepSimulationMinutes / 60f);
        
        if (carDistancePerStep < currentPosition.getDistanceRemaining()) {
            
            // We're not going to arrive to city this step, 
            // safely reduce remaining distance to city
            currentPosition.setDistanceRemaining(currentPosition.getDistanceRemaining() - carDistancePerStep);
            readyForPurging = false;
            
        } else {
            
            do {
                
                // We've definitely arrived to the city, check order list
                City arrivedTo = currentPosition.getOnRoadTo();
                int unloadedPallets = resolveOrders(arrivedTo);
                
                // ...but how much are we OVER the city? 
                float kmOverflow = carDistancePerStep - currentPosition.getDistanceRemaining();
                int minuteOverflow = (int) ((kmOverflow / currentPosition.getSpeed()) * 60f);
                
                if (unloadedPallets > 0) {
                    
                    int minutesForUnloading = unloadedPallets * MINUTES_TO_UNLOAD_SINGLE_PALLET;
                    
                    controller.events.add(
                            new SimulationTime(
                                    controller.time,
                                    +(currentStepLength-minuteOverflow)),
                            SimulationEvents.Type.CAR_UNLOADING,
                            String.format(
                                    "#%s started unloading %d pallets in %s (it will take %d minutes)",
                                    this, unloadedPallets, arrivedTo.getName(), minutesForUnloading)
                    );
                    
                    if (minuteOverflow > minutesForUnloading) {     // Unloading fits into overflow
                        minuteOverflow -= minutesForUnloading;      // Only decrease minute overflow
                        
                        controller.events.add(
                                new SimulationTime(
                                        controller.time,
                                        +(currentStepLength-minuteOverflow)),
                                SimulationEvents.Type.ORDER_RESOLVED,
                                String.format(
                                        "#%s finished unloading in %s.",
                                        this, arrivedTo.getName())
                        );
                        
                    } else {                                        // ...or if unloading won't fit...
                        // Apply the time overflow to be used in the next step()
                        currentPosition.setMinutesLeft2Unload(minutesForUnloading - minuteOverflow);
                        // There are no more minutes overflowing, no minutes can be used for driving
                        minuteOverflow = 0;  
                    }
                }

                // Head towards the next city
                City nextCity = plannedPath.pollFirst();
                if (nextCity == null) {
                    readyForPurging = true;
                    return;
                }
                advanceToNextSegment(nextCity);

                // Update current position by overflow of distance
                float carDistanceOverflow = currentPosition.getSpeed() * (minuteOverflow / 60f);
                currentPosition.setDistanceRemaining(currentPosition.getDistanceRemaining() - carDistanceOverflow);
                
            } while (currentPosition.getDistanceRemaining() < 0);
        }
    }

    /**
     * Updates {@link #currentPosition} to next segment. This includes 
     * the {@code fromCity} and {@code onRoadTo} endpoints, 
     * road speed and remaining distance to city. 
     * 
     * @param from  Starting point of segment
     * @param to    Ending point of segment
     * @return  Does a road between the cities exist? 
     */
    private boolean advanceToNextSegment(City from, City to) {

        // Validation
        if (!from.getNeighbours().containsKey(to)) {
            readyForPurging = true;
            return false;
            // throw new RuntimeException("Car update error:\n Car has an invalid path. ");
        }
        
        // Setting endpoints
        currentPosition.setFromCity(from);
        currentPosition.setOnRoadTo(to);

        // Connecting road + speed 
        RoadData road = from.getNeighbours().get(to);
        currentPosition.setSpeed(road.getSpeed());
        currentPosition.setDistanceRemaining(road.getDistance());
        
        return true;
    }

    /** 
     * @see #advanceToNextSegment(City, City) 
     * @param nextCity next city to be advanced to?
     * @return true if road between cities exists?
     * */
    private boolean advanceToNextSegment(City nextCity) {
        return advanceToNextSegment(currentPosition.getOnRoadTo(), nextCity);
    }

    /**
     * Resolves orders in the {@code currentCity}. 
     * @param currentCity  City to check orders in 
     * @return  Number of unloaded pallets
     */
    private int resolveOrders(City currentCity) {
        
        int unloaded = 0;
        LinkedList<Order> finishedOrders = new LinkedList<>();
        
        for (Order o : assignedOrders) {
            
            if (o.getTarget().equals(currentCity)) {
                
                if (o.getRequiredPallets() > this.nLoadedPallets) {
                    System.out.println("" +
                        "Error order resolving:\n" +
                        "  Car cannot complete assigned order, not enough pallets loaded on car.");
                    System.out.println("  Car: " + this + "; City: "+ o.getTarget());
                    continue;  // Don't break in case there is another order in the same city
                }
                
                
                unloaded += o.getRequiredPallets();        // Count how much has been unloaded
                nLoadedPallets -= o.getRequiredPallets();  // Unload
                
                finishedOrders.add(o);                // Mark order as finished
                
                // Counting the order summaries in controller
                controller._completedOrders++;
                controller._deliveredPallets += o.getRequiredPallets();
            }
            
        }
        
        assignedOrders.removeAll(finishedOrders);
        return unloaded;
    }

    @Override
    public String toString() {
        String memoryId = super.toString();  // Default ID from Object.toString()
        int len = memoryId.length();
        int digits = 4;
        
        return memoryId.substring(len-digits, len);
    }
}
