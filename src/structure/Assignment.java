package structure;
import java.util.LinkedList;
import java.util.List;

import simulation.SimulationTime;

/**
 * Class holds info about what a Car is going to carry
 */
public class Assignment {
	/** Pallets loaded onto a Car */
	private int nLoadedPallets;
	
	/** Its assigned orders */
	private LinkedList<Order> assignedOrders;
	
	/** Planned rout through the Map */
	private LinkedList<City> plannedPath;
	
	/** List of departure times from all ordering cities */
	private LinkedList<SimulationTime> departureTimes;
	
	
	/**
	 * @return the nLoadedPallets
	 */
	public int getnLoadedPallets() {
		return nLoadedPallets;
	}


	/**
	 * @param nLoadedPallets the nLoadedPallets to set
	 */
	public void setnLoadedPallets(int nLoadedPallets) {
		this.nLoadedPallets = nLoadedPallets;
	}


	/**
	 * @return the assignedOrders
	 */
	public LinkedList<Order> getAssignedOrders() {
		return assignedOrders;
	}


	/**
	 * @param assignedOrders the assignedOrders to set
	 */
	public void setAssignedOrders(LinkedList<Order> assignedOrders) {
		this.assignedOrders = assignedOrders;
	}


	/**
	 * @return the plannedPath
	 */
	public LinkedList<City> getPlannedPath() {
		return plannedPath;
	}


	/**
	 * @param plannedPath the plannedPath to set
	 */
	public void setPlannedPath(LinkedList<City> plannedPath) {
		this.plannedPath = plannedPath;
	}


	/**
	 * @return the departureTimes
	 */
	public LinkedList<SimulationTime> getDepartureTimes() {
		return departureTimes;
	}


	/**
	 * @param departureTimes the departureTimes to set
	 */
	public void setDepartureTimes(LinkedList<SimulationTime> departureTimes) {
		this.departureTimes = departureTimes;
	}


	/**
	 * Constructor - calls new on Lists and sets pallets to 0
	 */
	public Assignment() {
		this.nLoadedPallets = 0;
		this.assignedOrders = new LinkedList<>();
		this.plannedPath = new LinkedList<>();
		this.departureTimes = new LinkedList<>();
	}
}
