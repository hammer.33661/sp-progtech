package structure;
import java.util.ArrayList;
import java.util.Arrays;

import data.AreaModel;

/**
 * The Floyd-Warshall algorithm is used to find the shortest path between
 * all pairs of nodes in a weighted graph with either positive or negative
 * edge weights but without negative edge cycles.
 * 
 * The running time of the algorithm is O(n^3), being n the number of nodes in
 * the graph.
 *
 * 
 * @see <a href='https://github.com/kennyledet/Algorithm-Implementations/blob/master/Floyd_Warshall/Java/dalleng/FloydWarshall.java'>
 * 			FW algorithm</a>
 */
public class FloydWarshall {

    /** Matrix containing distances between each pair of Cities */
    static int[][] graphDistances;
    
    /** Matrix containing speeds between each pair of Cities */
    static int[][] graphSpeeds;
    
    /** List of two matrices */
    public ArrayList<int[][]> result;

    /**
     * Constructor of the FloydWarshall class calls for weight filling from
     * the AreaModel class - those results points to it's two matrices
     */
    public FloydWarshall() {
    	ArrayList<int[][]> w = AreaModel.fillWeights();
    	
    	graphDistances = w.get(0);
    	graphSpeeds = w.get(1);
    }

    /**
     * Does the actual work - claculates all speeds and distances using the FW algorithm
     * @return List of two matrices
     */
    public ArrayList<int[][]> floydWarshall() {
    	result = new ArrayList<>(2);
    	int n = graphDistances.length; //same for both
    	int[][] distances;
    	int[][] speeds;
    
    	distances = Arrays.copyOf(graphDistances, n);
    	speeds = Arrays.copyOf(graphSpeeds, n);
    	
         
        //
    	// since we have no control over the fact that the rout from
    	// two connected cities can be longer than going 5 cities around,
    	// at least we make the algorithm keep the distance between two
    	// neigbouring cities as ideal - pomocná matice tells us where 
    	// we (don't) want to 'make improvements'
    	boolean[][] pomocnaMatice = new boolean[n][n];
    	for(int i = 0; i < n; i++) {
    		for(int j = i; j < n; j++) {
    			if(distances[i][j] < Double.POSITIVE_INFINITY) {
    				pomocnaMatice[i][j] = false;
    				pomocnaMatice[j][i] = false;
    			}
    			else {
    				pomocnaMatice[i][j] = true;
    				pomocnaMatice[j][i] = true;
    			}
    		}
    	}
    	
    	

        for (int k = 0; k < n; k++) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                	
                	if(pomocnaMatice[i][j] == true) {
                		distances[i][j] = Math.min(distances[i][j], distances[i][k] + distances[k][j]);
                        speeds[i][j] = Math.min(speeds[i][j], speeds[i][k] + speeds[k][j]);
                	}
                }
            }
            
        }

        result.add(distances);
        result.add(speeds);
        
        return result;
    }

}
