package structure;
import java.util.ArrayList;
import java.util.HashMap;

import data.RoadData;

/**
 * Class represents a single city located somewhere in AreaModel
 */
public class City {

    /** A map of neighbouring cityMap with costs of travelling to them */
    private HashMap<City, RoadData> neighbours;  // Tie weight to next node

    /** Name of the city */
    private String name;

    /** The cost of traversing to city,
     *  temporary attribute for shortest path searching */
    private int cost;
    
    /** The City's ID used to connect Cities together */
    private int id;
    
    /** Maximum quantity of pallets the City can order */
    private int maxPallets;

    /** The shortest path to this city,
     *  temporary attribute */
    private ArrayList<City> shortestPath;
    
    /** Hour at which the city is ready to start accepting orders */ 
    private int openingHour;
    
    /** Hour at which the city closes */
    private int closingHour;


    /**
	 * @return the neighbours
	 */
	public HashMap<City, RoadData> getNeighbours() {
		return neighbours;
	}

	/**
	 * @param neighbours the neighbours to set
	 */
	public void setNeighbours(HashMap<City, RoadData> neighbours) {
		this.neighbours = neighbours;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the cost
	 */
	public int getCost() {
		return cost;
	}

	/**
	 * @param cost the cost to set
	 */
	public void setCost(int cost) {
		this.cost = cost;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the maxPallets
	 */
	public int getMaxPallets() {
		return maxPallets;
	}

	/**
	 * @param maxPallets the maxPallets to set
	 */
	public void setMaxPallets(int maxPallets) {
		this.maxPallets = maxPallets;
	}

	/**
	 * @return the shortestPath
	 */
	public ArrayList<City> getShortestPath() {
		return shortestPath;
	}

	/**
	 * @param shortestPath the shortestPath to set
	 */
	public void setShortestPath(ArrayList<City> shortestPath) {
		this.shortestPath = shortestPath;
	}

	/**
	 * @return the openingHour
	 */
	public int getOpeningHour() {
		return openingHour;
	}

	/**
	 * @param openingHour the openingHour to set
	 */
	public void setOpeningHour(int openingHour) {
		this.openingHour = openingHour;
	}

	/**
	 * @return the closingHour
	 */
	public int getClosingHour() {
		return closingHour;
	}

	/**
	 * @param closingHour the closingHour to set
	 */
	public void setClosingHour(int closingHour) {
		this.closingHour = closingHour;
	}

	/**
     * The instance represents a city in the model. It has its own opening and closing
     * hour and a maximum of pallets the city can want. The city holds a list
     * of its neighbours and the roads connecting them to the neighbouring cities. 
     * 
     * @param name        City's name
     * @param id          Identification number of the city
     * @param maxPallets  The maximal number of pallets this city can order
     * @param openingHour Hour the city opens (starts to accept pallets)
     * @param closingHour Hour the city closes (doesn't accept pallets anymore)
     */
    public City(String name, int id, int maxPallets, int openingHour, int closingHour) {

        if (name.trim().length() == 0)
            throw new IllegalArgumentException(
                    "Chyba vytváření města:\n Název města není validní. ("+ name +")");

        this.name = name;
        this.neighbours = new HashMap<City, RoadData>();
        this.id = id;
        this.maxPallets = maxPallets;
        this.openingHour = openingHour;
        this.closingHour = closingHour;
    }

    /**
     * Adds a neighbour to this city.
     * 
     * @param c  Neighbouring city
     * @param w  Weights of the road to the neighbouring city
     */
    public void connectTo(City c, RoadData w) {
        this.neighbours.put(c, w);
        c.neighbours.put(this, w);
    }

    @Override
    public String toString() { return "City("+ name + ")"; }

}
