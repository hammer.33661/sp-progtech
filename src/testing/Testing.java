package testing;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.LinkedList;

import data.AreaModel;
import data.RoadData;
import simulation.CliControl;
import simulation.Controller;
import simulation.SimulationEvents;
import simulation.SimulationTime;
import simulation.SimulationEvents.Event;
import simulation.SimulationEvents.Type;
import structure.City;
import structure.Order;

/**
 * A class used for testing
 *
 */
abstract class Testing {
    
    public static String DATA_FILE = "src/generator/data.txt";

    public static void main(String[] args) {
        // test_graph();
        // test_controller();
        test_carflow();
        // test_eventQ();
    }

    private static void test_controller() {

        AreaModel g = new AreaModel();
        g.setCityMap(new HashMap<>());

        City a, b, c, d;

        g.getCityMap().put(1, (a = new City("A", 1, 6, 10, 12)));
        g.getCityMap().put(2, (b = new City("B", 2, 6, 10, 12)));
        g.getCityMap().put(3, (c = new City("C", 3, 6, 10, 12)));
        g.getCityMap().put(4, (d = new City("Station", 4, 6, 10, 12)));

        a.connectTo(b, new RoadData(2,2));
        a.connectTo(c, new RoadData(3,3));
        b.connectTo(c, new RoadData(4,4));
        b.connectTo(d, new RoadData(1,1));
        c.connectTo(d, new RoadData(2,2));


        Controller control = new Controller(g, d);
        int reqPallets = 4;

        Order order = new Order(a, reqPallets);
        LinkedList<Order> orders = new LinkedList<>();
        orders.add(order);

        LinkedList<City> path = new LinkedList<>();
        path.add(d);
        path.add(c);
        path.add(a);
        path.add(b);
        path.add(d);

        //control.sendNewCar(4, orders, path);

        new CliControl(control, System.in);
    }
    
    private static void test_carflow() {
        AreaModel g = AreaModel.generateFromFile(DATA_FILE);
        int n = g.getCityMap().size();
        
        City station = g.getCityMap().get(0);
        City orderingCity = g.getCityMap().get(1);
        
        LinkedList<Order> orders = new LinkedList<>();
        orders.add(new Order(orderingCity, 6));
        System.out.println(station);
        System.out.println(orders);
        
        LinkedList<City> path = new LinkedList<>();
        path.add(station);
        path.addAll(g.getShortestPathBetween(station, orderingCity));
        path.addAll(g.getShortestPathBetween(orderingCity, station));
        
        Controller control = new Controller(g, station);
        control.sendNewCar(6, orders, path, null);
        
        new CliControl(control, System.in);
    }

    private static void test_graph() {
        AreaModel g =  AreaModel.generateFromFile(DATA_FILE);

        /*
        AreaModel g = new AreaModel();
        g.cityMap = new HashMap<>();
        
        City a, b, c, d;

        g.cityMap.put(1, (a = new City("A", 1, 6, 10, 12)));
        g.cityMap.put(2, (b = new City("B", 2, 6, 10, 12)));
        g.cityMap.put(3, (c = new City("C", 3, 6, 10, 12)));
        g.cityMap.put(4, (d = new City("Station", 4, 6, 10, 12)));

        a.connectTo(b, new RoadData(2,2));
        a.connectTo(c, new RoadData(3,3));
        b.connectTo(c, new RoadData(4,4));
        b.connectTo(d, new RoadData(1,1));
        c.connectTo(d, new RoadData(2,2));
        */
        
        int n = g.getCityMap().size();
        ArrayList<City> path = new ArrayList<>();
        City a = null;
        City b = null;
        
        for(int i = 0; i < 2000; i++) {
        	a = g.getCityMap().get((int) (Math.random() * n));
            b = g.getCityMap().get((int) (Math.random() * n));
            
            path.addAll(g.getShortestPathBetween(a, b));
        }
        

        System.out.format("%s -> %s: \n", a, b);
        for (City C : path)
            System.out.println(C.getName() + " -> ");
    }
    
    private static void test_eventQ() {
        
        SimulationEvents queue = new SimulationEvents();
        
        queue.add(
                new SimulationTime(3,00), 
                SimulationEvents.Type.CAR_INITIATED_PATH, 
                "Car@3333 started."
        );
        
        queue.add(
                new SimulationTime(3,45), 
                SimulationEvents.Type.CAR_PROGRESS, 
                "Car@3333 going strong!"
        );
        
        queue.add(
                new SimulationTime(3,55), 
                SimulationEvents.Type.CAR_DONE, 
                "Car@3333 finished! "
        );
        
        queue.add(
                new SimulationTime(3,20), 
                SimulationEvents.Type.CAR_INITIATED_PATH, 
                "Car@77 started.."
        );
        
        queue.add(
                new SimulationTime(3,30), 
                SimulationEvents.Type.CAR_UNLOADING, 
                "Car@77 is unloading.."
        );
        
        queue.add(
                new SimulationTime(3,40), 
                SimulationEvents.Type.CAR_PROGRESS, 
                "Car@77 is returning.."
        );
        
        queue.add(
                new SimulationTime(4,00),
                SimulationEvents.Type.CAR_DONE, 
                "Car@77 finished!!"
        );
        
        
        ArrayList<SimulationEvents.Event> sorted = queue.getEventsSortedByTime();
        for (SimulationEvents.Event e : sorted)
            System.out.println(e);
        
    }
}
