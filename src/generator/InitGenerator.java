package generator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Generator of cities, roads and their attributes
 * 
 * Creates file data.txt containing all needed data
 * The file structure is (number = line number):
 * 
 * 1 number of cities, number of roads
 * 2 ---
 * 3-2002 city id:city name,no. of pallets (order),open since,open till
 * 2003 ---
 * 2004-(2004+no. of cities) city1,city2,distance,time
 */
public class InitGenerator {
	
	/**
	 * The earties cities can start accepting orders */
	public final static int START_OKNO_MIN = 8;
	
	/** The latest cities can start accepting orders */
	public final static int START_OKNO_MAX = 18;
	
	/** Array of cities */
	static ArrayList<String> mesta = new ArrayList<>();
	
	/** Array of integers representing number of roads leading from cities */
	static int[] pocetCestZMest ;
	
	/** No. of cities */
	static int pocetMest;
	
	/** No. of roads */
	static int pocetCest;
	
	/** Relative file paths */ 
	static String cestaMesta = "generator/jenomMesta.txt";
	
	/** Relative file paths */
	static String cestaMestaCesty = "generator/mestaCesty.txt";
	
	/** Final data file */
	static String dataFile = "generator/data.txt";
	
	 /** Random instance */
	static Random rand = new Random();
	

	public static void main(String[] args) {
		BufferedReader rd = null;
		BufferedWriter wr = null;
		
		
		try {
			rd = new BufferedReader(new FileReader(cestaMesta));
			prectiMesta(rd);
			rd.close();
			
			pocetCestZMest = new int[pocetMest];
			
			wr = new BufferedWriter(new FileWriter(cestaMestaCesty));
			
			generujAtributy(wr);
			
			//prázdná řádka mezi městama a cestama
			wr.write("\n");
			
			generujCesty(wr);
			wr.close();
			
			rd = new BufferedReader(new FileReader(cestaMestaCesty));
			wr = new BufferedWriter(new FileWriter(dataFile));
			
			generujDataFile(rd, wr);
			
			
			rd.close();
			wr.close();
			
			File f = new File(cestaMestaCesty);
			f.delete();
			
		} catch (FileNotFoundException e) {
			System.out.println("Nepodařilo se načíst soubor s městy.");
			e.printStackTrace();
			
		} catch (IOException e) {
			System.out.println("Nepodařilo se načíst otevřít nebo uložit s městy.");
			e.printStackTrace();
		}
		
		System.out.println(" --- Map generated. --- ");
		
	}

	/**
	 * Generates data file 
	 * @param rd class BufferedReader instance 
	 * @param wr class BufferedWriter instance
	 * @throws IOException if anything goes wrong with file opening
	 */
	private static void generujDataFile(BufferedReader rd, BufferedWriter wr) throws IOException {
		
		String currentLine = rd.readLine();
		wr.write((int)pocetMest + "," + (int)pocetCest + "\n\n");
		
		while(currentLine != null) {
			wr.write(currentLine + "\n");
			currentLine = rd.readLine();
			
		}
		
	}


	/**
	 * Randomly generates paths between cities formated: from,where,distance in km,speed in km/h
	 * 
	 * Generates random indexes to 'neigbourhood' matrix so that
	 * the connections are as evenly distributed as possible
	 * 
	 * Checks whether at least 200 roads lead from each city
	 * 
	 * @param wr class BufferedWriter instance
	 * @throws IOException if anything goes wrong with file opening
	 */
	private static void generujCesty(BufferedWriter wr) throws IOException {
		int[][] maticeSoused = new int[pocetMest][pocetMest];
		//int pocetHran = 0;
		int randJ;
		int randI;
		pocetCest = 0;
		
		int randRychlost;
		int randVzdalenost;
		
		int minSpeed = 40;
		int maxSpeed = 120;
		
		
		while(!jeDostCest()) {
			randI = rand.nextInt(pocetMest);
			
			while(pocetCestZMest[randI] < 200) {
				randJ = rand.nextInt(pocetMest);
				
				if(randI == randJ) {
					maticeSoused[randI][randJ] = 0;
					maticeSoused[randJ][randI] = 0;
				}
				else if(maticeSoused[randI][randJ] == 0) {
					
					pocetCestZMest[randI] = pocetCestZMest[randI] + 1;
					pocetCestZMest[randJ] = pocetCestZMest[randJ] + 1;
					
					maticeSoused[randI][randJ] = 1;
					maticeSoused[randJ][randI] = 1;
					pocetCest++;
					
				}
			}
			
		}

		/*
		 * vymyslí random délku cesty (mezi 20 a 50 km) + její rychlost (v km/h), která je mezi 40 a 120 km/h
		 */
		for(int i = 0; i < pocetMest; i++) {
			for(int j = i; j < pocetMest; j++) {
				if(maticeSoused[i][j] == 1) {
					randVzdalenost = 20 + rand.nextInt(50 - 20 + 1); //města jsou 20 až 50 km daleko od sebe
					randRychlost = minSpeed + rand.nextInt(maxSpeed - minSpeed + 1); //car speed is between 40 and 140 kmph
					
					wr.write((int)i + "," + (int)j + "," + randVzdalenost + "," + randRychlost);
					wr.write("\n");
					
				}
			}
		}
		
	}

	/**
	 * Using {@code pocetCestZMest} checks, whether at least 200 roads lead from all cities
	 * @return true if at least 200 roads lead from all cities
	 */
	private static boolean jeDostCest() {
		boolean ret = true;
		
		for(int i = 0; i < pocetCestZMest.length; i++) {
			if(pocetCestZMest[i] < 200) {
				ret = false;
				return ret;
			}
		}
		return ret;
	}
	
	/**
	 * Prepares an ArrayList of Integers of 2000 numbers, where
	 * 25% of numbers are number 1
	 * another 25% of numbers are number 2
	 * another 20% of numbers are number 3
	 * another 15% of numbers are number 4
	 * another 10% of numbers are number 5
	 * another 5% of numbers are number 6.
	 * 
	 * The numbers represent quantity of pallets a City can order (maximum).
	 * Then shuffles the List.  
	 * @return shuffeled ArrayList
	 */
	private static ArrayList<Integer> naplnPocetPalet() {
		ArrayList<Integer> ret = new ArrayList<>();
		int i = 0;
		
		for(i = 0; i < 2000/4; i++) {
			ret.add(1);
		}
		
		for(i = 0; i < 2000/4; i++) {
			ret.add(2);
		}
		
		for(i = 0; i < 2000/5; i++) {
			ret.add(3);
		}
		
		for(i = 0; i < 20 * 15; i++) {
			ret.add(4);
		}
		
		for(i = 0; i < 200; i++) {
			ret.add(5);
		}
		
		for(i = 0; i < 20 * 5; i++) {
			ret.add(6);
		}
		
		Collections.shuffle(ret);
		
		return ret;
		
	}



	/**
	 * Adds attributes to cities
	 * 
	 * @param wr BufferedWriter writes to file
	 * @throws IOException if anything goes wrong with file opening
	 */
	private static void generujAtributy(BufferedWriter wr) throws IOException {
		String mesto;
		int randHodiny = 0;
		ArrayList<Integer> pocetPalet = naplnPocetPalet();

		for(int i = 0; i < mesta.size(); i++) {
			randHodiny = START_OKNO_MIN + rand.nextInt(START_OKNO_MAX - START_OKNO_MIN + 1);
					
			mesto = mesta.get(i);
			wr.write((int)(i) + ":" + mesto + "," + (int)pocetPalet.get(i) + "," + randHodiny + "," + (int)(randHodiny + 2) + "\n");
			
		}
		
	}

	/**
	 * Saves cities from file to {@code ArrayList<String> mesta}
	 * @param rd BufferedReader reading from file
	 * @throws IOException if anything goes wrong with file opening
	 */
	private static void prectiMesta(BufferedReader rd) throws IOException {
		
		String currentLine = rd.readLine();
		
		while(currentLine != null) {
			mesta.add(currentLine);
			currentLine = rd.readLine();
			pocetMest++;
			
		}
		
	}

}
